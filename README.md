# Wmux

Wmux is a proof of concept "screen" for Wayland, that makes it possible
to display the same program on multiple different compositors, and
dynamically add/remove which compositors a client is connected to.
Compare with Xpra [0].

How to use:

    ./wmux.py --local --display wayland-1 -- kwrite

This will create two windows. To dynamically add and remove windows,
write lines `connect wayland-2` or `drop cl2` into the pipe `wmux_conn_pipe`
to connect to the display `wayland-2`, or remove the third display that
was connected to. It is possible to connect to the same compositor multiple
times.

The program run under Wmux is connected via the environment variable
WAYLAND_SOCKET, with all its limitations.

Using wmux in conjunction with waypipe[1] makes it possible to keep a GUI program
running on a remote server, and periodically connect and disconnect to it.

[0] [https://www.xpra.org/](https://www.xpra.org/)
[1] [https://gitlab.freedesktop.org/mstoeckl/waypipe/](https://gitlab.freedesktop.org/mstoeckl/waypipe/)

# Bugs

Wmux has many bugs. Some are race conditions, so trying something twice may
help. Some are memory leaks, so don't run Wmux for too long. Many Wayland 
programs also have bugs, and have not been tested with multiple (or zero)
seats and outputs, so even if Wmux were perfect, programs would still
misbehave.

# Requirements

Python >=3.3.

libwayland and wayland-protocols should be installed to `/usr`; edit `wmux.py` to
adjust paths otherwise.

# License

The main code, `wmux.py`, is provided under GPLv3.

The `wayland` folder is a modified copy of python-wayland [2], and is under
the MIT license. It doesn't change very often, so someday I'll try to
upstream it.

[2] [https://github.com/sde1000/python-wayland](https://github.com/sde1000/python-wayland)
