"""Wayland protocol server implementation"""

import wayland.protocol
import os
import socket
import select
import struct
import array
import io

class ClientDisconnected(Exception):
    """The server disconnected unexpectedly"""
    pass


class _Display:
    """Additional methods for wl_display interface proxy

    The wl_display proxy class obtained by loading the Wayland
    protocol XML file needs to be augmented with some additional
    methods to function as a full Wayland protocol client.
    
    This should be provided with the socket file descriptor of the
    initial connection, and a str->interface map including all interfaces
    that clients may create via wl_registry::bind.
    """
    def __init__(self, socket, interfaces, prefix=None):
        self.prefix = prefix
        self.interfaces = interfaces
        
        self._oids = iter(range(0xff000000, 0xffffffff))
        self._reusable_oids = []
        self._default_queue = []
        super(_Display, self).__init__(self, 1, # the display is object 1
                                       self._default_queue, 1)
        
        self._f = socket # a socket object
        self.log.info("connected to existing fd %d", self._f.fileno())
        
        self._f.setblocking(0)
        
        # Partial event left from last read
        self._read_partial_request = b''
        self._incoming_fds = []

        self.objects = {self.oid: self}
        self._send_queue = []
        
        # for proxy self-reference
        self.display = self
        
        self.dispatcher['sync'] = self._default_sync

        self.serial = 0

        #self.dispatcher['delete_id'] = self._delete_id
        #self.silence['delete_id'] = True
        #self.dispatcher['error'] = self._error_event
        
        pass
    
    def next_serial(self):
        self.serial += 1
        return self.serial
    
    def _default_sync(self, display, callback):
        callback.done(self.next_serial())
    
    def _get_new_oid(self):
        if self._reusable_oids:
            return self._reusable_oids.pop()
        return next(self._oids)
    
    def __del__(self):
        self.disconnect()

    def disconnect(self):
        """Disconnect from the server.

        Closes the socket.  After calling this method, all further
        calls to this proxy or any other proxies on the connection
        will fail.
        """
        if self._f:
            self._f.close()
            self._f = None
    
    def _delete_id(self, display, id_):
        self.log.info("client deleted %s", self.objects[id_])
        self.objects[id_].oid = None
        del self.objects[id_]
        if id_ >= 0xff000000:
            self._reusable_oids.append(id_)

    def _queue_event(self, r, fds=[]):
        self.log.debug("queueing to send: %s with fds %s", r, fds)
        self._send_queue.append((r, fds))
    
    def flush(self):
        """Send buffered requests to the client

        Will send as many requests as possible to the client.
        Will not block; if sendmsg() would block, will leave events in
        the queue.

        Returns True if the queue was emptied.
        """
        while self._send_queue:
            b, fds = self._send_queue.pop(0)
            try:
                nbytes = self._f.sendmsg([b], [(socket.SOL_SOCKET, socket.SCM_RIGHTS,
                                       array.array("i", fds))])
                assert nbytes == len(b), "partial message write"
                for fd in fds:
                    os.close(fd)
            except socket.error as e:
                if socket.errno == 11:
                    # Would block.  Return the data to the head of the queue
                    # and try again later!
                    self.log.debug("flush would block; returning data to queue")
                    self._send_queue.insert(0, (b, fds))
                    return False
                raise
        return True
    
    def recv(self):
        """Receive as much data as is available.

        Returns True if any data was received.  Will not block.
        """
        data = None
        try:
            fds = array.array("i")
            data, ancdata, msg_flags, address = self._f.recvmsg(
                1024, socket.CMSG_SPACE(16 * fds.itemsize))
            for cmsg_level, cmsg_type, cmsg_data in ancdata:
                if (cmsg_level == socket.SOL_SOCKET and
                    cmsg_type == socket.SCM_RIGHTS):
                    fds.frombytes(cmsg_data[
                        :len(cmsg_data) - (len(cmsg_data) % fds.itemsize)])
            self._incoming_fds.extend(fds)
            if data:
                self._decode(data)
                return True
            else:
                raise ClientDisconnected()
        except socket.error as e:
            if e.errno == 11:
                # No data available; would otherwise block
                return False
            raise

    def dispatch(self):
        """Dispatch the default event queue.

        If the queue is empty, block until events are available and
        dispatch them.
        """
        self.flush()
        while not self._default_queue:
            select.select([self._f], [], [])
            self.recv()
        self.dispatch_pending()

    def dispatch_pending(self, queue=None):
        """Dispatch pending events in an event queue.

        If queue is None, dispatches from the default event queue.
        Will not read from the server connection.
        """
        if not queue:
            queue = self._default_queue
        while queue:
            e = self._default_queue.pop(0)
            if isinstance(e, Exception):
                raise e
            proxy, event, args = e
            proxy.dispatch_request(event, args)


    def _decode(self, data):
        # There may be partial event data already received; add to it
        # if it's there
        if self._read_partial_request:
            data = self._read_partial_request + data
        #print("initial:", len(data))
        while len(data) >= 8:
            oid, sizeop = struct.unpack("II", data[0 : 8])
            
            size = sizeop >> 16
            op = sizeop & 0xffff

            if len(data) < size:
                self.log.debug("partial request received: %d byte request, "
                               "%d bytes available", size, len(data))
                break

            argdata = io.BytesIO(data[8 : size])
            data = data [size : ]

            obj = self.objects.get(oid, None)
            #print(obj, size)
            if obj:
                with argdata:
                    e = obj._unmarshal_request(op, argdata, self._incoming_fds)
                    self.log.debug(
                        "queueing request: %s(%d) %s %s",
                        e[0].interface.name, e[0].oid, e[1].name, e[2])
                    obj.queue.append(e)
            else:
                obj.queue.append(UnknownObjectError(obj))
        self._read_partial_request = data
        #print("remaining:", len(self._read_partial_request))

def MakeDisplay(protocol):
    """Create a Display class from a Wayland protocol definition

    Args:
        protocol: a wayland.protocol.Protocol instance containing a
        core Wayland protocol definition.

    Returns:
        A Display proxy class built from the specified protocol.
    """
    # Q: shouldn't the proxy class be given to the ClientDisplay?
    # our ideal interface is basically to make ClientDisplays independent channels
    
    # TODO: another option, have "MakeDisplay" be passed in an already primed connection? So that our displays are really 1-1 matches to Client displays.
    # after all, this "display" object is the one that clients have.
    # there's no reason at all to synchronize globals.
    
    class Display(_Display, protocol['wl_display'].server_proxy_class):
        pass
    return Display
