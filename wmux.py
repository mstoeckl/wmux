#!/usr/bin/env python3
#
# Copyright 2020 M Stoeckl
#
# This file is part of Wmux.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

"""
Wayland forwarding compositor, which dynamically combines a variable
number of input compositors. Welcome to callback hell.

Guide to function names:

type_EVT_eventname  : process event from upstream
type_REQ_eventname  : process request from program
type_DROP           : remove an upstream object for a display
type_CLEAR          : cleans up all object corresponding to a downstream program
type_ADD            : add an upstream object (if it doesn't already exist)
_type_blah          : helper function

Common attributes:
.upstream(s)        : Corresponding object(s) on compositor side
.downstream(s)      : Corresponding object(s) on program side
.center             : The unique global linked to a compositor/program implementation

# todo: names to distinguish upstream and downstream types? uwl_shm, dwl_shm ?
"""

import wayland.protocol
import wayland.client
import wayland.server
import wayland.utils
import os
import logging
import argparse
import copy
import socket
import random
import subprocess
import time
import select
import sys

log = logging.getLogger(__name__)


class LoopHook:
    def __init__(self, display, wproxy):
        self.display = display
        self.wproxy = wproxy

    def fileno(self):
        return self.display._f.fileno()

    def doread(self):
        try:
            self.display.recv()
        except wayland.client.ServerDisconnected:
            self.wproxy.remove_client(self.display.prefix)
            return
        except wayland.server.ClientDisconnected:
            # exit the event loop
            self.wproxy.remove_server(self.display.prefix)
            return
        except ConnectionResetError:
            if type(self.display) == self.wproxy.ClientDisplay:
                self.wproxy.remove_client(self.display.prefix)
                return
            else:
                self.wproxy.remove_server(self.display.prefix)
                return

        self.display.dispatch_pending()

    def _preselect(self):
        try:
            # TODO: handle retries and BlockingIOError appropriately...
            self.display.flush()
        except BrokenPipeError:
            if type(self.display) == self.wproxy.ClientDisplay:
                self.wproxy.remove_client(self.display.prefix)
                return
            else:
                self.wproxy.shutdowncode = 1
                return


class DynamicGlobal:
    """
    A wayland global object, like wl_seat or wl_output, which corresponds to an
    actual thing provided by the compositor that can be dynamically added and removed,
    and which may exist any number of times, including zero.

    Each global only has one upstream object, but may be bound multiple times.

    Additional properties are to be set as needed by the user.
    """

    def __init__(self, name, reg_adv_name=None):
        self.global_name = name
        self.reg_adv_name = reg_adv_name
        self.upstream = None
        self.downstreams = []

    def __repr__(self):
        return "dglobal:" + self.global_name


class StaticGlobal:
    """
    A wayland global object, like xdg_wm_base, or wl_compositor, which by itself
    has no state (ignoring e.g. format lists), and which will never be offered
    more or less than once by a compositor supporting it.
    """

    def __init__(self, name, reg_adv_name=None):
        self.global_name = name
        self.reg_adv_name = reg_adv_name
        self.upstreams = {}  # map [cdisp]->object
        self.downstreams = []

    def __repr__(self):
        return "sglobal:" + self.global_name


def _link_updown(downstream, upstream):
    """
    Helper function, often used when adding an upstream.
    Call like:
    upstream = _link_updown(downstream, new_upstream_object)
    """
    cdisp = upstream.display
    assert cdisp not in downstream.upstreams, (downstream, upstream)
    downstream.upstreams[cdisp] = upstream
    upstream.downstream = downstream

    return upstream


def _link_up_center(center, upstream):
    """
    Helper function, often used when adding an upstream.
    Call like:
    upstream = _link_updown(center, new_upstream_object)
    """
    cdisp = upstream.display
    assert cdisp not in center.upstreams, (center, upstream)
    center.upstreams[cdisp] = upstream
    upstream.center = center

    return upstream


def _create_static_global_upstream(glob, cdisp):
    """
    Create an upstream object for a StaticGlobal, and add crosslinks
    to the global
    """
    reg = glob.registry
    try:
        uname, uversion = _registry_globlist_version_for_name(
            reg.upstreams[cdisp], glob.global_name
        )
    except KeyError:
        return None
    upglob = reg.upstreams[cdisp].bind(
        uname, cdisp.interfaces[glob.global_name], uversion
    )
    _link_up_center(glob, upglob)
    return upglob


def _wl_callback_sync_common(main_callback):
    if all(
        (subcallback.done_yet is not None)
        for cdisp, subcallback in main_callback.upstreams.items()
    ):
        # print("Subcallbacks done", list(main_callback.upstreams.values()))
        latest_dserial = max(
            subcallback.done_yet
            for cdisp, subcallback in main_callback.upstreams.items()
        )
        main_callback.done(latest_dserial)
        # the callback is destroyed after the "done" event. TODO fix official protocol docs
        main_disp = main_callback.display.upstream
        main_disp.sync_callbacks.remove(main_callback)


def wl_callback_sync_DROP(wl_callback, cdisp):
    # This callback was created by a wl_display_sync call
    upstream = wl_callback.upstreams[cdisp]
    del wl_callback.upstreams[cdisp]
    upstream.downstream = None
    # Check again
    _wl_callback_sync_common(wl_callback)


def wl_callback_sync_ADD(wl_callback, cdisp):
    # The fact that we are calling this means that the callback has not yet returned
    # print("Queueing subcallback")
    subcallback = cdisp.sync()
    _link_updown(callback, subcallback)
    subcallback.done_yet = None
    subcallback.dispatcher["done"] = wl_callback_sync_EVT_done


def wl_callback_sync_EVT_done(wl_callback, serial):
    dserial = wl_callback.downstream.display.serial_downshift(
        wl_callback.display, serial
    )
    wl_callback.done_yet = dserial
    _wl_callback_sync_common(wl_callback.downstream)


def wl_display_REQ_sync(wl_display, callback):
    main_disp = wl_display.upstream
    # Forward the round trip out to the compositors we are connected to,
    # and call back when the last one is ready
    if len(main_disp.upstreams) == 0:
        callback.done(wl_display.next_down_serial())
    else:
        main_disp.sync_callbacks.append(callback)
        callback.upstreams = {}

        for cdisp in main_disp.upstreams:
            # print("Queueing subcallback")
            subcallback = cdisp.sync()
            _link_updown(callback, subcallback)
            subcallback.done_yet = None
            subcallback.dispatcher["done"] = wl_callback_sync_EVT_done


def wl_shm_pool_REQ_destroy(wl_shm_pool):
    # TODO: actually keep the pool fd around, to "keep the tree alive" so we can build
    # it back up again later
    for cdisp in wl_shm_pool.upstreams:
        wl_shm_pool.upstreams[cdisp].destroy()


def wl_shm_pool_REQ_resize(wl_shm_pool, size):
    wl_shm_pool.pool_size = size
    # TODO: verify FD size
    for cdisp in wl_shm_pool.upstreams:
        wl_shm_pool.upstreams[cdisp].resize(size)


def wl_buffer_REQ_destroy(wl_buffer):
    # deregister
    if wl_buffer.buftype == "shm":
        wl_buffer.pool.buffers.remove(wl_buffer)
    elif wl_buffer.buftype == "dmabuf_immed":
        wl_buffer.params.buffers.remove(wl_buffer)
    else:
        raise NotImplementedError()

    # Unlink from all attachments
    for surf in wl_buffer.committed_surfaces:
        assert surf.committed_state.wl_buffer == wl_buffer
        surf.committed_state.wl_buffer = None
    for surf in wl_buffer.pending_surfaces:
        assert surf.pending_state.wl_buffer == wl_buffer
        surf.pending_state.wl_buffer = None

    for cdisp in wl_buffer.upstreams:
        wl_buffer.upstreams[cdisp].destroy()


def wl_buffer_EVT_release(wl_buffer):
    # Each upstream, we assume, calls release at most once per time the buffer
    # is attached (for real, via commit)
    if wl_buffer.display in wl_buffer.downstream.pending_releases:
        wl_buffer.downstream.pending_releases.remove(wl_buffer.display)
    else:
        print("Possible duplicate release event with", wl_buffer)
    if not len(wl_buffer.downstream.pending_releases):
        wl_buffer.downstream.release()


def wl_buffer_ADD(wl_buffer, cdisp):
    if cdisp in wl_buffer.upstreams:
        return

    if wl_buffer.buftype == "shm":
        wl_shm_pool = wl_buffer.pool
        offset, width, height, stride, fmt = wl_buffer.create_args

        wl_shm_pool_ADD(wl_shm_pool, cdisp)
        if cdisp in wl_buffer.upstreams:
            return
        u = wl_shm_pool.upstreams[cdisp].create_buffer(
            offset, width, height, stride, fmt
        )
    elif wl_buffer.buftype == "dmabuf_immed":
        zwp_linux_buffer_params_v1 = wl_buffer.params
        width, height, fmt, flags = wl_buffer.create_args

        zwp_linux_dmabuf_params_v1_ADD(zwp_linux_buffer_params_v1, cdisp)
        if cdisp in wl_buffer.upstreams:
            return
        u = zwp_linux_buffer_params_v1.upstreams[cdisp].create_immed(
            width, height, fmt, flags
        )
    else:
        # todo: support late dmabuf!
        raise NotImplementedError("Unknown wl_buffer creation method")

    _link_updown(wl_buffer, u)
    u.dispatcher["release"] = wl_buffer_EVT_release

    # If the wl_buffer is attached to a wl_surface, it is the responsibility of that
    # surface to reattach the buffer to it when we call wl_buffer_ADD , since the
    # surface_ADD function calls buffer_ADD early on


def _wl_buffer_make_common(wl_buffer):
    wl_buffer.dispatcher["destroy"] = wl_buffer_REQ_destroy

    # Which surfaces the buffer has been attached but not committed
    wl_buffer.pending_surfaces = []
    # Which surfaces the buffer is currently active on
    wl_buffer.committed_surfaces = []
    # Which upstream displays have we committed this attached buffer to but which have
    # not yet sent a release notification?
    wl_buffer.pending_releases = set()


def wl_buffer_DROP(wl_buffer, cdisp):
    if cdisp in wl_buffer.pending_releases:
        wl_buffer.pending_releases.remove(cdisp)
        if len(wl_buffer.pending_releases) == 0:
            wl_buffer.release()

    del wl_buffer.upstreams[cdisp]


def wl_shm_pool_REQ_create_buffer(
    wl_shm_pool, wl_buffer, offset, width, height, stride, fmt
):
    wl_shm_pool.buffers.append(wl_buffer)

    # note: wl_buffer can also be created by dmabuf...
    wl_buffer.buftype = "shm"
    wl_buffer.pool = wl_shm_pool
    wl_buffer.create_args = (offset, width, height, stride, fmt)

    _wl_buffer_make_common(wl_buffer)

    wl_buffer.upstreams = {}
    for cdisp in wl_shm_pool.upstreams:
        wl_buffer_ADD(wl_buffer, cdisp)


def wl_shm_pool_DROP(wl_shm_pool, cdisp):
    for buf in wl_shm_pool.buffers:
        wl_buffer_DROP(buf, cdisp)

    del wl_shm_pool.upstreams[cdisp]


def wl_shm_pool_ADD(wl_shm_pool, cdisp):
    if cdisp in wl_shm_pool.upstreams:
        return
    wl_shm = wl_shm_pool.shm
    wl_shm_ADD(wl_shm, cdisp)

    p = wl_shm.upstreams[cdisp].create_pool(wl_shm_pool.pool_fd, wl_shm_pool.pool_size)
    _link_updown(wl_shm_pool, p)

    for buf in wl_shm_pool.buffers:
        wl_buffer_ADD(buf, cdisp)


def wl_shm_REQ_create_pool(wl_shm, wl_shm_pool, fdno, size):
    wl_shm.center.pools.append(wl_shm_pool)
    wl_shm_pool.dispatcher["resize"] = wl_shm_pool_REQ_resize
    wl_shm_pool.dispatcher["create_buffer"] = wl_shm_pool_REQ_create_buffer
    wl_shm_pool.dispatcher["destroy"] = wl_shm_pool_REQ_destroy

    wl_shm_pool.pool_fd = fdno
    wl_shm_pool.pool_size = size
    wl_shm_pool.buffers = []
    wl_shm_pool.shm = wl_shm.center

    wl_shm_pool.upstreams = {}
    for cdisp in wl_shm.center.upstreams:
        wl_shm_pool_ADD(wl_shm_pool, cdisp)


def wl_shm_DROP(wl_shm, cdisp):
    upshm = wl_shm.upstreams[cdisp]
    del wl_shm.upstreams[cdisp]
    for pool in wl_shm.pools:
        wl_shm_pool_DROP(pool, cdisp)


def wl_shm_CLEAR(wl_shm, sdisp):
    pool_list = [x for x in wl_shm.pools if x.display == sdisp]
    for pool in pool_list:
        for buf in pool.buffers:
            wl_buffer_REQ_destroy(buf)
        if False:  # todo, fix logic here
            # user hasn't yet deleted...
            wl_shm_pool_REQ_destroy(pool)

    # temporary hard remove until shm pool refcounting ready
    wl_shm.pools = [x for x in wl_shm.pools if x.display != sdisp]


def wl_surface_REQ_destroy(wl_surface):
    pbuf = wl_surface.pending_state.wl_buffer
    cbuf = wl_surface.committed_state.wl_buffer
    if cbuf is not None:
        cbuf.committed_surfaces.remove(wl_surface)
    if pbuf is not None:
        pbuf.pending_surfaces.remove(wl_surface)

    # TODO: handle toplevel disconnection? wipe internal state?

    for cdisp in wl_surface.upstreams:
        wl_surface.upstreams[cdisp].destroy()

    wl_surface.compositor.surfaces.remove(wl_surface)


def wl_surface_REQ_attach(wl_surface, wl_buffer, x, y):
    # Which buffer is attached is double-buffered state; a buffer can be attached
    # to a large number of surfaces simultaneously, but as per documentation
    # how wl_buffer.release is handled in such a case is undefined. The most
    # correct policy would be to maintain a reference count to all surfaces
    # which have a committed attachment. (A "pending" attachment does not by itself
    # warrant a release; so we maintain separate current/committed lists)
    if wl_surface.pending_state.wl_buffer is not None:
        wl_surface.pending_state.wl_buffer.pending_surfaces.remove(wl_surface)

    wl_buffer.pending_surfaces.append(wl_surface)
    wl_surface.pending_state.wl_buffer = wl_buffer
    wl_surface.pending_state.wl_buffer_attach_x = x
    wl_surface.pending_state.wl_buffer_attach_y = y

    for cdisp in wl_surface.upstreams:
        if (
            wl_surface.xdg_surface
            and not wl_surface.xdg_surface.upstreams[cdisp].has_been_configured
        ):
            # Do not attach any surfaces until configure is done; we will make first attachment then
            continue

        # TODO: keep track of all attachments, so we can properly synchronize buffer releases
        wl_surface.upstreams[cdisp].attach(wl_buffer.upstreams[cdisp], x, y)


def wl_surface_REQ_damage(wl_surface, x, y, width, height):
    wl_surface.pending_state.damage_list.append((x, y, width, height))
    for cdisp in wl_surface.upstreams:
        wl_surface.upstreams[cdisp].damage(x, y, width, height)


def wl_callback_frame_REQ_done(wl_callback, serial):
    dcb = wl_callback.downstream
    if dcb.have_replied:
        return
    dserial = dcb.display.serial_downshift(wl_callback.display, serial)

    # As soon as the first callback returns, trigger & clear
    # everything associated with the surface
    for callback in dcb.surface.frame_callbacks:
        callback.done(dserial)
        callback.have_replied = True
    dcb.surface.frame_callbacks = []


def wl_surface_REQ_frame(wl_surface, callback):
    wl_surface.frame_callbacks.append(callback)
    callback.surface = wl_surface

    # The downstream callback returns as soon as the first upstream callback does.
    # Consequently, the fastest upstream effectively controls the refresh rate
    callback.have_replied = False

    for cdisp in wl_surface.upstreams:
        subcallback = wl_surface.upstreams[cdisp].frame()
        subcallback.dispatcher["done"] = wl_callback_frame_REQ_done
        subcallback.downstream = callback


def wl_surface_REQ_set_opaque_region(wl_surface, wl_region):
    wl_surface.pending_state.opaque_region = wl_region
    for cdisp in wl_surface.upstreams:
        wl_surface.upstreams[cdisp].set_opaque_region(
            wl_region.upstreams[cdisp] if wl_region is not None else None
        )


def wl_surface_REQ_set_input_region(wl_surface, wl_region):
    wl_surface.pending_state.input_region = wl_region
    for cdisp in wl_surface.upstreams:
        wl_surface.upstreams[cdisp].set_input_region(
            wl_region.upstreams[cdisp] if wl_region is not None else None
        )


def wl_surface_REQ_damage_buffer(wl_surface, x, y, width, height):
    wl_surface.pending_state.damage_buffer_list.append((x, y, width, height))
    for cdisp in wl_surface.upstreams:
        wl_surface.upstreams[cdisp].damage_buffer(x, y, width, height)


def wl_surface_REQ_set_buffer_transform(wl_surface, transform):
    wl_surface.pending_state.buffer_transform = transform
    for cdisp in wl_surface.upstreams:
        wl_surface.upstreams[cdisp].set_buffer_transform(transform)


def wl_surface_REQ_set_buffer_scale(wl_surface, scale):
    wl_surface.pending_state.buffer_scale = scale
    for cdisp in wl_surface.upstreams:
        wl_surface.upstreams[cdisp].set_buffer_scale(scale)


def wl_surface_DROP(wl_surface, cdisp):
    # Q: what about connected xdg surfaces, and buffers?
    if wl_surface.committed_state is not None:
        cbuf = wl_surface.committed_state.wl_buffer
        if cbuf is not None:
            if cdisp in cbuf.pending_releases:
                cbuf.pending_releases.remove(cdisp)
                if len(cbuf.pending_releases) == 0:
                    cbuf.release()

    # TODO: pending frame() callbacks, if we drop the last surface...
    # (or maybe figure out how to resume when upstream reconnects?
    # so, put a linked frame() call into wl_surface_ADD ?
    # note: if we call frame() multiple times, then we can address
    # all the frame() callbacks as a bundle. So we need a list
    # of 'frame callbacks', which themselves split into upstreams,
    # and as soon as any upstream goes, we trigger them all.

    if cdisp in wl_surface.upstreams:
        wl_surface.upstreams[cdisp].downstream = None
        del wl_surface.upstreams[cdisp]


class WlSurfaceState:
    def __init__(self):
        self.wl_buffer = None
        self.wl_buffer_attach_x = None
        self.wl_buffer_attach_y = None

        self.buffer_scale = None
        self.buffer_transform = None
        self.damage_list = []
        self.damage_buffer_list = []
        self.opaque_region = None
        self.input_region = None

        # XDG surface state, which is stored/applied only for XDG surfaces...
        self.title = None


def _wl_surface_run_init(wl_surface, cdisp):
    upsurf = wl_surface.upstreams[cdisp]

    # The first commit requires that no wl_buffer be attached, so go ahead.
    if wl_surface.committed_state is not None:
        cs = wl_surface.committed_state
        if cs.buffer_scale is not None:
            upsurf.set_buffer_scale(cs.buffer_scale)
        if cs.buffer_transform is not None:
            upsurf.set_buffer_transform(cs.buffer_transform)
        if cs.opaque_region is not None:
            upsurf.set_opaque_region(
                cs.opaque_region.upstreams[cdisp]
                if cs.opaque_region is not None
                else None
            )
        if cs.input_region is not None:
            upsurf.set_input_region(
                cs.input_region.upstreams[cdisp]
                if cs.input_region is not None
                else None
            )
        for x, y, width, height in cs.damage_list:
            upsurf.damage(x, y, width, height)
        for x, y, width, height in cs.damage_buffer_list:
            upsurf.damage_buffer(x, y, width, height)

        if wl_surface.xdg_surface:
            upxdg = wl_surface.xdg_surface.upstreams[cdisp]
            if cs.title is not None:
                upxdg.set_title(cs.title)

        # first time, no extra attachments
        upsurf.commit()

    # Next, try to update everything to the current state. The wl_buffer need not exist
    # yet, which is fine -- if this thing has already been created, it will attach itself;
    # if it hasn't, it won't.
    os = wl_surface.committed_state
    cs = wl_surface.pending_state
    if cs.buffer_scale is not None and not (os and cs.buffer_scale == os.buffer_scale):
        upsurf.set_buffer_scale(cs.buffer_scale)
    if cs.buffer_transform is not None and not (
        os and cs.buffer_transform == os.buffer_transform
    ):
        upsurf.set_buffer_transform(cs.buffer_transform)
    if cs.opaque_region is not None and not (
        os and cs.opaque_region == os.opaque_region
    ):
        upsurf.set_opaque_region(
            cs.opaque_region if cs.opaque_region is not None else None
        )
    if cs.input_region is not None and not (os and cs.input_region == os.input_region):
        upsurf.set_input_region(
            cs.input_region if cs.input_region is not None else None
        )
    for x, y, width, height in cs.damage_list:
        upsurf.damage(x, y, width, height)
    for x, y, width, height in cs.damage_buffer_list:
        upsurf.damage_buffer(x, y, width, height)

    if wl_surface.xdg_surface:
        upxdg = wl_surface.xdg_surface.upstreams[cdisp]
        if cs.title is not None:
            upxdg.set_title(cs.title)

    # And attach buffer, now that first commit is done. (Or wait until configure received?)
    if cs.wl_buffer is not None:
        # Ensure buffer's upstream is available
        wl_buffer_ADD(cs.wl_buffer, cdisp)

        upsurf.attach(
            cs.wl_buffer.upstreams[cdisp],
            cs.wl_buffer_attach_x,
            cs.wl_buffer_attach_y,
        )

    # Queue any frame callbacks which we have asked for
    if len(wl_surface.frame_callbacks):
        subcallback = wl_surface.upstreams[cdisp].frame()
        subcallback.dispatcher["done"] = wl_callback_frame_REQ_done
        # it doesn't matter which one we point to
        subcallback.downstream = wl_surface.frame_callbacks[0]


def wl_surface_ADD(wl_surface, cdisp):
    if cdisp in wl_surface.upstreams:
        return
    wl_compositor_ADD(wl_surface.compositor, cdisp)
    if cdisp in wl_surface.upstreams:
        return

    # First off, always create the surface object, so that others (xdg_surface) may reference it
    upsurf = wl_surface.compositor.upstreams[cdisp].create_surface()
    _link_updown(wl_surface, upsurf)

    if wl_surface.xdg_surface:
        # The creation of the xdg_surface may have been waiting on this.
        xdg_surface_ADD(wl_surface.xdg_surface, cdisp)
    else:
        # no role, may as well go ahead blindly?
        _wl_surface_run_init(wl_surface, cdisp)


def wl_compositor_REQ_create_surface(compositor, wl_surface):
    compositor.center.surfaces.append(wl_surface)
    wl_surface.upstreams = {}
    wl_surface.compositor = compositor.center

    # We assume that a given surface is never assigned to more than one xdg_surface over its lifetime....
    wl_surface.xdg_surface = None
    # Commands modify 'current' state; when we commit, the committed state is updated to match
    wl_surface.pending_state = WlSurfaceState()
    wl_surface.committed_state = None
    # List of all the frame callbacks which are pending
    wl_surface.frame_callbacks = []

    wl_surface.dispatcher["destroy"] = wl_surface_REQ_destroy
    wl_surface.dispatcher["commit"] = wl_surface_REQ_commit
    wl_surface.dispatcher["attach"] = wl_surface_REQ_attach
    wl_surface.dispatcher["damage"] = wl_surface_REQ_damage
    wl_surface.dispatcher["frame"] = wl_surface_REQ_frame
    wl_surface.dispatcher["set_opaque_region"] = wl_surface_REQ_set_opaque_region
    wl_surface.dispatcher["set_input_region"] = wl_surface_REQ_set_input_region
    wl_surface.dispatcher["set_buffer_transform"] = wl_surface_REQ_set_buffer_transform
    wl_surface.dispatcher["set_buffer_scale"] = wl_surface_REQ_set_buffer_scale
    wl_surface.dispatcher["damage_buffer"] = wl_surface_REQ_damage_buffer

    for cdisp in compositor.center.upstreams:
        wl_surface_ADD(wl_surface, cdisp)


def wl_compositor_DROP(wl_compositor, cdisp):
    for surface in wl_compositor.surfaces:
        wl_surface_DROP(surface, cdisp)

    wl_compositor.upstreams[cdisp].downstream = None
    del wl_compositor.upstreams[cdisp]


def wl_compositor_CLEAR(wl_compositor, sdisp):
    surf_list = [x for x in wl_compositor.surfaces if x.display == sdisp]
    for surf in surf_list:
        wl_surface_REQ_destroy(surf)


def wl_subcompositor_REQ_get_subsurface(
    wl_subcompositor, wl_subsurface, surface, parent
):
    raise NotImplementedError()


def wl_subcompositor_DROP(wl_subcompositor, cdisp):
    # todo: drop upstreams for subsurfaces

    wl_subcompositor.upstreams[cdisp].downstream = None
    del wl_subcompositor.upstreams[cdisp]


def wl_subcompositor_CLEAR(wl_compositor, sdisp):
    # todo: clear matching subsurfaces
    pass


def zxdg_toplevel_decoration_v1_DROP(zxdg_toplevel_decoration_v1, cdisp):
    # it is not guaranteed that the upstream supports this
    if cdisp in zxdg_toplevel_decoration_v1.upstreams:
        del zxdg_toplevel_decoration_v1.upstreams[cdisp]


def zxdg_toplevel_decoration_v1_REQ_destroy(zxdg_toplevel_decoration_v1):
    for cdisp in zxdg_toplevel_decoration_v1.upstreams:
        zxdg_toplevel_decoration_v1.upstreams[cdisp].destroy()

    zxdg_toplevel_decoration_v1.toplevel.decoration = None
    zxdg_toplevel_decoration_v1.manager.decorations.remove(zxdg_toplevel_decoration_v1)


def zxdg_toplevel_decoration_v1_REQ_set_mode(zxdg_toplevel_decoration_v1, mode):
    zxdg_toplevel_decoration_v1.preferred_mode = mode
    for cdisp in zxdg_toplevel_decoration_v1.upstreams:
        zxdg_toplevel_decoration_v1.upstreams[cdisp].set_mode(mode)


def zxdg_toplevel_decoration_v1_REQ_unset_mode(zxdg_toplevel_decoration_v1):
    zxdg_toplevel_decoration_v1.preferred_mode = None
    for cdisp in zxdg_toplevel_decoration_v1.upstreams:
        zxdg_toplevel_decoration_v1.upstreams[cdisp].unset_mode()


def zxdg_toplevel_decoration_v1_ADD(zxdg_toplevel_decoration_v1, cdisp):
    if cdisp in zxdg_toplevel_decoration_v1.upstreams:
        return
    zxdg_decoration_manager_v1_ADD(zxdg_toplevel_decoration_v1.manager, cdisp)
    if cdisp not in zxdg_toplevel_decoration_v1.manager.upstreams:
        # This global is not available, so do not add anything
        return
    xdg_toplevel_ADD(zxdg_toplevel_decoration_v1.toplevel, cdisp)
    if cdisp in zxdg_toplevel_decoration_v1.upstreams:
        return

    updeco = zxdg_toplevel_decoration_v1.manager.upstreams[
        cdisp
    ].get_toplevel_decoration(zxdg_toplevel_decoration_v1.toplevel.upstreams[cdisp])
    _link_updown(zxdg_toplevel_decoration_v1, updeco)

    # TODO: what about configure event, and double buffered state?
    if zxdg_toplevel_decoration_v1.preferred_mode is not None:
        updeco.set_mode(zxdg_toplevel_decoration_v1.preferred_mode)


def zxdg_decoration_manager_v1_REQ_get_toplevel_decoration(
    zxdg_decoration_manager_v1, decoration, toplevel
):
    zxdg_decoration_manager_v1.center.decorations.append(decoration)
    decoration.manager = zxdg_decoration_manager_v1.center
    decoration.toplevel = toplevel
    toplevel.decoration = decoration

    decoration.dispatcher["destroy"] = zxdg_toplevel_decoration_v1_REQ_destroy
    decoration.dispatcher["set_mode"] = zxdg_toplevel_decoration_v1_REQ_set_mode
    decoration.dispatcher["unset_mode"] = zxdg_toplevel_decoration_v1_REQ_unset_mode
    decoration.preferred_mode = None  # or enum client/server side

    decoration.upstreams = {}
    for cdisp in zxdg_decoration_manager_v1.center.upstreams:
        zxdg_toplevel_decoration_v1_ADD(decoration, cdisp)


def zxdg_decoration_manager_v1_DROP(zxdg_decoration_manager_v1, cdisp):
    for deco in zxdg_decoration_manager_v1.decorations:
        zxdg_toplevel_decoration_v1_DROP(deco, cdisp)
    # It is not guaranteed that the upstream compositor has this protocol
    if cdisp in zxdg_decoration_manager_v1.upstreams:
        zxdg_decoration_manager_v1.upstreams[cdisp].downstream = None
        del zxdg_decoration_manager_v1.upstreams[cdisp]


def zxdg_decoration_manager_v1_CLEAR(zxdg_decoration_manager_v1, sdisp):
    deco_list = [
        x for x in zxdg_decoration_manager_v1.decorations if x.display == sdisp
    ]
    for deco in deco_list:
        zxdg_toplevel_decoration_v1_REQ_destroy(deco)


def wl_region_REQ_destroy(wl_region):
    for cdisp in wl_region.upstreams:
        wl_region.upstreams[cdisp].destroy()


def wl_region_REQ_add(region, x, y, width, height):
    region.change_log.append(("ADD", x, y, width, height))
    for cdisp in region.upstreams:
        region.upstreams[cdisp].add(x, y, width, height)


def wl_region_REQ_subtract(region, x, y, width, height):
    region.change_log.append(("SUB", x, y, width, height))
    for cdisp in region.upstreams:
        region.upstreams[cdisp].subtract(x, y, width, height)


def wl_region_ADD(region, cdisp):
    if cdisp in region.upstreams:
        return
    wl_compositor_ADD(region.compositor, cdisp)
    if cdisp in region.upstreams:
        return

    upcomp = region.compositor.upstreams[cdisp]
    ureg = upcomp.create_region()
    _link_updown(region, ureg)

    for change, x, y, width, height in region.change_log:
        if change == "ADD":
            ureg.add(x, y, width, height)
        elif change == "SUB":
            ureg.sub(x, y, width, height)
        else:
            raise NotImplementedError()


def wl_compositor_REQ_create_region(compositor, region):
    compositor.center.regions.append(region)
    region.compositor = compositor.center
    region.upstreams = {}
    region.change_log = []

    for cdisp in compositor.center.upstreams:
        wl_region_ADD(region, cdisp)

    region.dispatcher["add"] = wl_region_REQ_add
    region.dispatcher["subtract"] = wl_region_REQ_subtract
    region.dispatcher["destroy"] = wl_region_REQ_destroy


def wl_surface_REQ_commit(wl_surface):
    if wl_surface.committed_state is not None:
        old_cbuf = wl_surface.committed_state.wl_buffer
        if old_cbuf is not None:
            wl_surface.committed_state.wl_buffer.committed_surfaces.remove(wl_surface)

    wl_surface.committed_state = wl_surface.pending_state
    wl_surface.pending_state = copy.copy(wl_surface.committed_state)
    # reset damage
    wl_surface.pending_state.damage_list = []
    wl_surface.pending_state.damage_buffer_list = []

    new_cbuf = wl_surface.committed_state.wl_buffer
    if new_cbuf is not None:
        new_cbuf.committed_surfaces.append(wl_surface)

    for cdisp in wl_surface.upstreams:
        # Q: do we still wait on xdg_surface, or should we just have the _role_ be the one choosing when surface is committed?
        if (
            wl_surface.xdg_surface is not None
            and wl_surface.pending_state.wl_buffer is not None
            and not wl_surface.xdg_surface.upstreams[cdisp].has_been_configured
        ):
            # Skip the commit until we have configured something
            continue

        # Q: workaround for "xdg surface has not been configured issue"
        wl_surface.upstreams[cdisp].commit()
        if new_cbuf is not None:
            new_cbuf.pending_releases.add(cdisp)


def xdg_toplevel_REQ_destroy(xdg_toplevel):
    for cdisp in xdg_toplevel.upstreams:
        xdg_toplevel.upstreams[cdisp].destroy()


def xdg_toplevel_EVT_close(xdg_toplevel):
    xdg_toplevel.downstream.close()


def xdg_toplevel_EVT_configure(xdg_toplevel, width, height, array):
    xdg_toplevel.downstream.configure(width, height, array)


def xdg_surface_REQ_destroy(xdg_surface):
    for cdisp in xdg_surface.upstreams:
        xdg_surface.upstreams[cdisp].destroy()


def xdg_toplevel_REQ_set_title(xdg_toplevel, title):
    xdg_toplevel.the_title = title
    for cdisp in xdg_toplevel.upstreams:
        xdg_toplevel.upstreams[cdisp].set_title(title)


def xdg_toplevel_REQ_set_parent(xdg_toplevel, parent):
    for cdisp in xdg_toplevel.upstreams:
        xdg_toplevel.upstreams[cdisp].set_parent(
            parent.upstreams[cdisp] if parent is not None else None
        )


def xdg_toplevel_REQ_set_app_id(xdg_toplevel, app_id):
    xdg_toplevel.the_app_id = app_id
    for cdisp in xdg_toplevel.upstreams:
        xdg_toplevel.upstreams[cdisp].set_app_id(app_id)


def xdg_toplevel_REQ_show_window_menu(xdg_toplevel, seat, serial, x, y):
    updisp, userial = xdg_toplevel.display.serial_upshift(serial)
    if updisp != seat.upstream.display:
        print("Client tried to show window menu using serial from the wrong seat")
        return

    xdg_toplevel.upstreams[updisp].show_window_menu(seat.upstream, userial, x, y)


def xdg_toplevel_REQ_move(xdg_toplevel, seat, serial):
    updisp, userial = xdg_toplevel.display.serial_upshift(serial)
    if updisp != seat.upstream.display:
        print("Client tried to show move toplevel using serial from the wrong seat")
        return
    xdg_toplevel.upstreams[updisp].move(seat.upstream, userial)


def xdg_toplevel_REQ_resize(xdg_toplevel, seat, serial, edges):
    updisp, userial = xdg_toplevel.display.serial_upshift(serial)
    if updisp != seat.upstream.display:
        print("Client tried to start resize using serial from the wrong seat")
        return
    xdg_toplevel.upstreams[updisp].resize(seat.upstream, userial, edges)


def xdg_toplevel_REQ_set_max_size(xdg_toplevel, width, height):
    xdg_toplevel.the_max_size = (width, height)
    for cdisp in xdg_toplevel.upstreams:
        xdg_toplevel.upstreams[cdisp].set_max_size(width, height)


def xdg_toplevel_REQ_set_min_size(xdg_toplevel, width, height):
    xdg_toplevel.the_min_size = (width, height)
    for cdisp in xdg_toplevel.upstreams:
        xdg_toplevel.upstreams[cdisp].set_min_size(width, height)


def xdg_toplevel_REQ_set_maximized(xdg_toplevel):
    for cdisp in xdg_toplevel.upstreams:
        xdg_toplevel.upstreams[cdisp].set_maximized()


def xdg_toplevel_REQ_unset_maximized(xdg_toplevel):
    for cdisp in xdg_toplevel.upstreams:
        xdg_toplevel.upstreams[cdisp].unset_maximized()


def xdg_toplevel_REQ_set_fullscreen(xdg_toplevel, output):
    if output is None:
        for cdisp in xdg_toplevel.upstreams:
            xdg_toplevel.upstreams[cdisp].set_fullscreen(None)
    else:
        # todo: if output is not null, how exactly we deal with this depends on which compositors have the output
        # just make fullscreen only on the corresponding compositor?
        raise NotImplementedError()


def xdg_toplevel_REQ_unset_fullscreen(xdg_toplevel):
    for cdisp in xdg_toplevel.upstreams:
        xdg_toplevel.upstreams[cdisp].unset_fullscreen()


def xdg_toplevel_REQ_set_minimized(xdg_toplevel):
    for cdisp in xdg_toplevel.upstreams:
        xdg_toplevel.upstreams[cdisp].set_minimized()


def xdg_toplevel_ADD(xdg_toplevel, cdisp):
    if cdisp in xdg_toplevel.upstreams:
        return
    xdg_surface = xdg_toplevel.xdg_surface
    xdg_surface_ADD(xdg_surface, cdisp)
    if cdisp in xdg_toplevel.upstreams:
        return

    xup = xdg_surface.upstreams[cdisp].get_toplevel()
    _link_updown(xdg_toplevel, xup)
    xup.dispatcher["configure"] = xdg_toplevel_EVT_configure
    xup.dispatcher["close"] = xdg_toplevel_EVT_close

    if xdg_toplevel.the_title is not None:
        xup.set_title(xdg_toplevel.the_title)
    # Q: parent? fullscreen state
    if xdg_toplevel.the_app_id is not None:
        xup.set_app_id(xdg_toplevel.the_app_id)
    if xdg_toplevel.the_max_size is not None:
        xup.set_max_size(xdg_toplevel.the_max_size[0], xdg_toplevel.the_max_size[1])
    if xdg_toplevel.the_min_size is not None:
        xup.set_min_size(xdg_toplevel.the_min_size[0], xdg_toplevel.the_min_size[1])

    # Now that the toplevel role is available, we can commit() the current surface state
    _wl_surface_run_init(xdg_toplevel.xdg_surface.base_surface, cdisp)


def xdg_toplevel_DROP(xdg_toplevel, cdisp):
    del xdg_toplevel.upstreams[cdisp]


def xdg_surface_REQ_get_toplevel(xdg_surface, xdg_toplevel):
    xdg_toplevel.dispatcher["destroy"] = xdg_toplevel_REQ_destroy
    xdg_toplevel.dispatcher["set_title"] = xdg_toplevel_REQ_set_title
    xdg_toplevel.dispatcher["set_parent"] = xdg_toplevel_REQ_set_parent
    xdg_toplevel.dispatcher["set_app_id"] = xdg_toplevel_REQ_set_app_id
    xdg_toplevel.dispatcher["show_window_menu"] = xdg_toplevel_REQ_show_window_menu
    xdg_toplevel.dispatcher["move"] = xdg_toplevel_REQ_move
    xdg_toplevel.dispatcher["resize"] = xdg_toplevel_REQ_resize
    xdg_toplevel.dispatcher["set_max_size"] = xdg_toplevel_REQ_set_max_size
    xdg_toplevel.dispatcher["set_min_size"] = xdg_toplevel_REQ_set_min_size
    xdg_toplevel.dispatcher["set_maximized"] = xdg_toplevel_REQ_set_maximized
    xdg_toplevel.dispatcher["unset_maximized"] = xdg_toplevel_REQ_unset_maximized
    xdg_toplevel.dispatcher["set_fullscreen"] = xdg_toplevel_REQ_set_fullscreen
    xdg_toplevel.dispatcher["unset_fullscreen"] = xdg_toplevel_REQ_unset_fullscreen
    xdg_toplevel.dispatcher["set_minimized"] = xdg_toplevel_REQ_set_minimized

    xdg_toplevel.the_title = None
    xdg_toplevel.the_app_id = None
    xdg_toplevel.the_max_size = None
    xdg_toplevel.the_min_size = None

    xdg_surface.xdg_toplevel = xdg_toplevel
    xdg_toplevel.xdg_surface = xdg_surface

    xdg_toplevel.upstreams = {}
    for cdisp in xdg_surface.upstreams:
        xdg_toplevel_ADD(xdg_toplevel, cdisp)


def xdg_popup_REQ_destroy(xdg_popup):
    for cdisp in xdg_popup.upstreams:
        xdg_popup.upstreams[cdisp].destroy()


def xdg_popup_REQ_grab(xdg_popup, seat, serial):
    updisp, userial = xdg_popup.display.serial_upshift(serial)
    assert updisp == seat.center.upstream.display
    # Only forward this to the compositor owning the seat upstream
    xdg_popup.upstreams[updisp].grab(seat.center.upstream, userial)


def xdg_popup_EVT_configure(xdg_popup, x, y, width, height):
    xdg_popup.downstream.configure(x, y, width, height)


def xdg_popup_EVT_popup_done(xdg_popup):
    xdg_popup.downstream.popup_done()


def xdg_popup_ADD(xdg_popup, cdisp):
    if cdisp in xdg_popup.upstreams:
        return

    xdg_surface_ADD(xdg_popup.xdg_surface, cdisp)
    if xdg_popup.parent is not None:
        xdg_surface_ADD(xdg_popup.parent, cdisp)
    if xdg_popup.positioner is not None:
        xdg_positioner_ADD(xdg_popup.positioner, cdisp)

    # Q: what if parent surface is not available yet?
    xup = xdg_popup.xdg_surface.upstreams[cdisp].get_popup(
        (xdg_popup.parent.upstreams[cdisp] if xdg_popup.parent is not None else None),
        xdg_popup.positioner.upstreams[cdisp],
    )
    _link_updown(xdg_popup, xup)
    xup.dispatcher["popup_done"] = xdg_popup_EVT_popup_done
    xup.dispatcher["configure"] = xdg_popup_EVT_configure

    # Now that the toplevel role is available, we can commit() the current surface state
    _wl_surface_run_init(xdg_popup.xdg_surface.base_surface, cdisp)


def xdg_popup_DROP(xdg_popup, cdisp):
    del xdg_popup.upstreams[cdisp]


def xdg_surface_REQ_get_popup(xdg_surface, xdg_popup, parent, positioner):
    xdg_popup.dispatcher["destroy"] = xdg_popup_REQ_destroy
    xdg_popup.dispatcher["grab"] = xdg_popup_REQ_grab
    xdg_popup.upstreams = {}
    xdg_popup.parent = parent
    # TODO: technically, calling "get_popup" copies the entire state of the positioner
    # so we should store the entire state, and then when creating the popup, dynamically
    # make, submit, and destroy the corresponding positioner state. In other words,
    # an xdg_positioner should have no upstreams...
    xdg_popup.positioner = positioner

    xdg_popup.xdg_surface = xdg_surface
    xdg_surface.xdg_popup = xdg_popup

    for cdisp in xdg_surface.upstreams:
        xdg_popup_ADD(xdg_popup, cdisp)


def xdg_surface_EVT_configure(xdg_surface, serial):
    dserial = xdg_surface.downstream.display.serial_downshift(
        xdg_surface.display, serial
    )
    xdg_surface.downstream.configure(dserial)


def xdg_surface_REQ_ack_configure(xdg_surface, serial):
    updisp, userial = xdg_surface.display.serial_upshift(serial)
    # Only ack the configure for the display the serial came from
    xdg_surface.upstreams[updisp].ack_configure(userial)
    xdg_surface.upstreams[updisp].has_been_configured = True

    base_surf = xdg_surface.base_surface
    if base_surf.pending_state.wl_buffer:
        base_surf.upstreams[updisp].attach(
            base_surf.pending_state.wl_buffer.upstreams[updisp],
            base_surf.pending_state.wl_buffer_attach_x,
            base_surf.pending_state.wl_buffer_attach_y,
        )

        # TODO: if the downstream has already committed something with a surface,
        # then we should replicate the downstream state and then commit on this
        # upstream


def xdg_surface_REQ_set_window_geometry(xdg_surface, x, y, width, height):
    for cdisp in xdg_surface.upstreams:
        xdg_surface.upstreams[cdisp].set_window_geometry(x, y, width, height)


def xdg_surface_ADD(xdg_surface, cdisp):
    # skip if already created
    if cdisp in xdg_surface.upstreams:
        return

    # ensure prerequisites are available
    xdg_wm_base_ADD(xdg_surface.wm_base, cdisp)
    wl_surface_ADD(xdg_surface.base_surface, cdisp)
    # Second check, that this function has not been called recursively via xdg_wm_base...
    if cdisp in xdg_surface.upstreams:
        return

    xs = xdg_surface.wm_base.upstreams[cdisp].get_xdg_surface(
        xdg_surface.base_surface.upstreams[cdisp]
    )
    _link_updown(xdg_surface, xs)
    xs.dispatcher["configure"] = xdg_surface_EVT_configure

    xs.has_been_configured = False

    # the _wl_surface_run_init will be called by the toplevel/popup role surface
    if xdg_surface.xdg_toplevel is not None:
        xdg_toplevel_ADD(xdg_surface.xdg_toplevel, cdisp)
    if xdg_surface.xdg_popup is not None:
        xdg_popup_ADD(xdg_surface.xdg_popup, cdisp)


def xdg_surface_DROP(xdg_surface, cdisp):
    if xdg_surface.xdg_toplevel is not None:
        xdg_toplevel_DROP(xdg_surface.xdg_toplevel, cdisp)
    if xdg_surface.xdg_popup is not None:
        xdg_popup_DROP(xdg_surface.xdg_popup, cdisp)
    del xdg_surface.upstreams[cdisp]


def xdg_wm_base_REQ_get_xdg_surface(xdg_wm_base, xdg_surface, wl_surface):
    xdg_surface.dispatcher["destroy"] = xdg_surface_REQ_destroy
    xdg_surface.dispatcher["get_toplevel"] = xdg_surface_REQ_get_toplevel
    xdg_surface.dispatcher["get_popup"] = xdg_surface_REQ_get_popup
    xdg_surface.dispatcher["ack_configure"] = xdg_surface_REQ_ack_configure
    xdg_surface.dispatcher["set_window_geometry"] = xdg_surface_REQ_set_window_geometry

    xdg_wm_base.center.surfaces.append(xdg_surface)
    xdg_surface.upstreams = {}

    xdg_surface.base_surface = wl_surface
    xdg_surface.wm_base = xdg_wm_base.center
    wl_surface.xdg_surface = xdg_surface
    xdg_surface.xdg_toplevel = None
    xdg_surface.xdg_popup = None

    for cdisp in xdg_wm_base.center.upstreams:
        # Try to create surface, unless prerequsite wl_surface has not yet been made
        xdg_surface_ADD(xdg_surface, cdisp)


def xdg_wm_base_DROP(xdg_wm_base, cdisp):
    for surface in xdg_wm_base.surfaces:
        xdg_surface_DROP(surface, cdisp)
    for positioner in xdg_wm_base.positioners:
        xdg_positioner_DROP(positioner, cdisp)

    xdg_wm_base.upstreams[cdisp].downstream = None
    del xdg_wm_base.upstreams[cdisp]


def xdg_wm_base_CLEAR(xdg_wm_base, sdisp):
    surf_list = [x for x in xdg_wm_base.surfaces if x.display == sdisp]
    for surf in surf_list:
        if surf.xdg_toplevel:
            xdg_toplevel_REQ_destroy(surf.xdg_toplevel)
        if surf.xdg_popup:
            xdg_popup_REQ_destroy(surf.xdg_popup)
        xdg_surface_REQ_destroy(surf)
    posi_list = [x for x in xdg_wm_base.positioners if x.display == sdisp]
    for positioner in posi_list:
        xdg_positioner_REQ_destroy(posi)


def xdg_positioner_REQ_destroy(xdg_positioner):
    for cdisp in xdg_positioner.upstreams:
        xdg_positioner.upstreams[cdisp].destroy()


def xdg_positioner_REQ_set_size(xdg_positioner, width, height):
    xdg_positioner.the_size = (width, height)
    for cdisp in xdg_positioner.upstreams:
        xdg_positioner.upstreams[cdisp].set_size(width, height)


def xdg_positioner_REQ_set_anchor_rect(xdg_positioner, x, y, width, height):
    xdg_positioner.the_anchor_rect = (x, y, width, height)
    for cdisp in xdg_positioner.upstreams:
        xdg_positioner.upstreams[cdisp].set_anchor_rect(x, y, width, height)


def xdg_positioner_REQ_set_anchor(xdg_positioner, anchor):
    xdg_positioner.the_anchor = anchor
    for cdisp in xdg_positioner.upstreams:
        xdg_positioner.upstreams[cdisp].set_anchor(anchor)


def xdg_positioner_REQ_set_gravity(xdg_positioner, gravity):
    xdg_positioner.the_gravity = gravity
    for cdisp in xdg_positioner.upstreams:
        xdg_positioner.upstreams[cdisp].set_gravity(gravity)


def xdg_positioner_REQ_set_constraint_adjustment(xdg_positioner, constraint_adjustment):
    xdg_positioner.the_constraint_adjustment = constraint_adjustment
    for cdisp in xdg_positioner.upstreams:
        xdg_positioner.upstreams[cdisp].set_constraint_adjustment(constraint_adjustment)


def xdg_positioner_REQ_set_offset(xdg_positioner, x, y):
    xdg_positioner.the_offset = (x, y)
    for cdisp in xdg_positioner.upstreams:
        xdg_positioner.upstreams[cdisp].set_offset(x, y)


def xdg_positioner_REQ_set_reactive(xdg_positioner):
    xdg_positioner.is_reactive = True
    for cdisp in xdg_positioner.upstreams:
        xdg_positioner.upstreams[cdisp].set_reactive()


def xdg_positioner_REQ_set_parent_size(xdg_positioner, parent_width, parent_height):
    xdg_positioner.the_parent_size = (parent_width, parent_height)
    for cdisp in xdg_positioner.upstreams:
        xdg_positioner.upstreams[cdisp].set_parent_size(parent_width, parent_height)


def xdg_positioner_REQ_set_parent_configure(xdg_positioner, serial):
    updisp, userial = xdg_positioner.display.serial_upshift(serial)
    xdg_positioner.upstreams[updisp].set_parent_configure(userial)


def xdg_positioner_ADD(xdg_positioner, cdisp):
    if cdisp in xdg_positioner.upstreams:
        return
    xdg_wm_base_ADD(xdg_positioner.wm_base, cdisp)

    xup = xdg_positioner.wm_base.upstreams[cdisp].create_positioner()
    _link_updown(xdg_positioner, xup)

    if xdg_positioner.the_size is not None:
        xup.set_size(*xdg_positioner.the_size)
    if xdg_positioner.the_anchor_rect is not None:
        xup.set_anchor_rect(*xdg_positioner.the_anchor_rect)
    if xdg_positioner.the_anchor is not None:
        xup.set_anchor(xdg_positioner.the_anchor)
    if xdg_positioner.the_gravity is not None:
        xup.set_gravity(xdg_positioner.the_gravity)
    if xdg_positioner.the_constraint_adjustment is not None:
        xup.set_constraint_adjustment(xdg_positioner.the_constraint_adjustment)
    if xdg_positioner.the_offset is not None:
        xup.set_offset(*xdg_positioner.the_offset)
    if xdg_positioner.is_reactive:
        xup.set_reactive()
    if xdg_positioner.the_parent_size is not None:
        xup.set_parent_size(*xdg_positioner.the_parent_size)
    if xdg_positioner.the_parent_configure is not None:
        # Skip: the parent configure is only directed in response to a configure event
        pass


def xdg_positioner_DROP(xdg_positioner, cdisp):
    del xdg_positioner.upstreams[cdisp]


def xdg_wm_base_REQ_create_positioner(xdg_wm_base, xdg_positioner):
    xdg_positioner.dispatcher["destroy"] = xdg_positioner_REQ_destroy
    xdg_positioner.dispatcher["set_size"] = xdg_positioner_REQ_set_size
    xdg_positioner.dispatcher["set_anchor_rect"] = xdg_positioner_REQ_set_anchor_rect
    xdg_positioner.dispatcher["set_anchor"] = xdg_positioner_REQ_set_anchor
    xdg_positioner.dispatcher["set_gravity"] = xdg_positioner_REQ_set_gravity
    xdg_positioner.dispatcher[
        "set_constraint_adjustment"
    ] = xdg_positioner_REQ_set_constraint_adjustment
    xdg_positioner.dispatcher["set_offset"] = xdg_positioner_REQ_set_offset
    xdg_positioner.dispatcher["set_reactive"] = xdg_positioner_REQ_set_reactive
    xdg_positioner.dispatcher["set_parent_size"] = xdg_positioner_REQ_set_parent_size
    xdg_positioner.dispatcher[
        "set_parent_configure"
    ] = xdg_positioner_REQ_set_parent_configure

    xdg_wm_base.center.positioners.append(xdg_positioner)
    xdg_positioner.wm_base = xdg_wm_base.center

    # This is a fairly simple type, without references to other structures
    xdg_positioner.the_size = None
    xdg_positioner.the_anchor_rect = None
    xdg_positioner.the_anchor = None
    xdg_positioner.the_gravity = None
    xdg_positioner.the_constraint_adjustment = None
    xdg_positioner.the_offset = None
    xdg_positioner.is_reactive = False
    xdg_positioner.the_parent_size = None
    xdg_positioner.the_parent_configure = None

    xdg_positioner.upstreams = {}
    for cdisp in xdg_wm_base.center.upstreams:
        xdg_positioner_ADD(xdg_positioner, cdisp)


def wl_pointer_REQ_set_cursor(wl_pointer, serial, surface, hotspot_x, hotspot_y):
    updisp, userial = wl_pointer.display.serial_upshift(serial)

    if updisp != wl_pointer.upstream.display:
        print(
            "Client appears to have updated cursor in response to a serial from a wrong seat, ignoring",
            (updisp, wl_pointer.upstream.display),
        )
        return

    # TODO: set surface to point to this pointer; is uniqueness required?
    if surface is None:
        wl_pointer.upstream.set_cursor(userial, None, hotspot_x, hotspot_y)
    else:
        wl_pointer.upstream.set_cursor(
            userial,
            surface.upstreams[wl_pointer.upstream.display],
            hotspot_x,
            hotspot_y,
        )


def wl_pointer_EVT_enter(wl_pointer, serial, surface, surface_x, surface_y):
    dserial = wl_pointer.downstream.display.serial_downshift(wl_pointer.display, serial)
    # Keep track of who owns the current surface, so if we have multiple
    # programs we can selectively pass through input events to only the intended targets
    wl_pointer.current_surface_display = surface.downstream.display
    if wl_pointer.downstream.display == wl_pointer.current_surface_display:
        wl_pointer.downstream.enter(dserial, surface.downstream, surface_x, surface_y)


def wl_pointer_EVT_leave(wl_pointer, serial, surface):
    dserial = wl_pointer.downstream.display.serial_downshift(wl_pointer.display, serial)
    if wl_pointer.downstream.display == wl_pointer.current_surface_display:
        wl_pointer.downstream.leave(dserial, surface.downstream)
    wl_pointer.current_surface_display = None


def wl_pointer_EVT_motion(wl_pointer, time, surface_x, surface_y):
    if wl_pointer.downstream.display == wl_pointer.current_surface_display:
        wl_pointer.downstream.motion(time, surface_x, surface_y)


def wl_pointer_EVT_button(wl_pointer, serial, time, button, state):
    dserial = wl_pointer.downstream.display.serial_downshift(wl_pointer.display, serial)
    if wl_pointer.downstream.display == wl_pointer.current_surface_display:
        wl_pointer.downstream.button(dserial, time, button, state)


def wl_pointer_EVT_axis(wl_pointer, time, axis, value):
    if wl_pointer.downstream.display == wl_pointer.current_surface_display:
        wl_pointer.downstream.axis(time, axis, value)


def wl_pointer_EVT_frame(wl_pointer):
    if wl_pointer.downstream.display == wl_pointer.current_surface_display:
        if wl_pointer.downstream.version >= 5:
            wl_pointer.downstream.frame()


def wl_pointer_EVT_axis_source(wl_pointer, axis_source):
    if wl_pointer.downstream.display == wl_pointer.current_surface_display:
        wl_pointer.downstream.axis_source(axis_source)


def wl_pointer_EVT_axis_stop(wl_pointer, time, axis):
    if wl_pointer.downstream.display == wl_pointer.current_surface_display:
        wl_pointer.downstream.axis_stop(time, axis)


def wl_pointer_EVT_axis_discrete(wl_pointer, axis, discrete):
    if wl_pointer.downstream.display == wl_pointer.current_surface_display:
        wl_pointer.downstream.axis_discrete(axis, discrete)


def wl_pointer_REQ_release(wl_pointer):
    wl_pointer.upstream.release()
    wl_pointer.seat.pointers.remove(wl_pointer)


def wl_seat_REQ_get_pointer(seat, pointer):
    pointer.upstream = upoint = seat.center.upstream.get_pointer()
    upoint.downstream = pointer

    pointer.seat = seat.center
    seat.center.pointers.append(pointer)

    pointer.dispatcher["set_cursor"] = wl_pointer_REQ_set_cursor
    pointer.dispatcher["release"] = wl_pointer_REQ_release

    upoint.current_surface_display = None
    upoint.dispatcher["enter"] = wl_pointer_EVT_enter
    upoint.dispatcher["leave"] = wl_pointer_EVT_leave
    upoint.dispatcher["motion"] = wl_pointer_EVT_motion
    upoint.dispatcher["button"] = wl_pointer_EVT_button
    upoint.dispatcher["frame"] = wl_pointer_EVT_frame
    upoint.dispatcher["axis"] = wl_pointer_EVT_axis
    upoint.dispatcher["axis_source"] = wl_pointer_EVT_axis_source
    upoint.dispatcher["axis_stop"] = wl_pointer_EVT_axis_stop
    upoint.dispatcher["axis_discrete"] = wl_pointer_EVT_axis_discrete


def wl_keyboard_EVT_key(wl_keyboard, serial, time, key, state):
    dserial = wl_keyboard.downstream.display.serial_downshift(
        wl_keyboard.display, serial
    )
    if wl_keyboard.downstream.display == wl_keyboard.current_surface_display:
        wl_keyboard.downstream.key(dserial, time, key, state)


def wl_keyboard_EVT_modifiers(
    wl_keyboard, serial, mods_depressed, mods_latched, mods_locked, group
):
    dserial = wl_keyboard.downstream.display.serial_downshift(
        wl_keyboard.display, serial
    )
    if wl_keyboard.downstream.display == wl_keyboard.current_surface_display:
        wl_keyboard.downstream.modifiers(
            dserial, mods_depressed, mods_latched, mods_locked, group
        )


def wl_keyboard_EVT_enter(wl_keyboard, serial, surface, keys):
    dserial = wl_keyboard.downstream.display.serial_downshift(
        wl_keyboard.display, serial
    )
    wl_keyboard.current_surface_display = surface.downstream.display
    if wl_keyboard.downstream.display == wl_keyboard.current_surface_display:
        wl_keyboard.downstream.enter(dserial, surface.downstream, keys)


def wl_keyboard_EVT_leave(wl_keyboard, serial, surface):
    dserial = wl_keyboard.downstream.display.serial_downshift(
        wl_keyboard.display, serial
    )
    if wl_keyboard.downstream.display == wl_keyboard.current_surface_display:
        wl_keyboard.downstream.leave(dserial, surface.downstream)
    wl_keyboard.current_surface_display = None


def wl_keyboard_EVT_repeat_info(keyboard, repeat_rate, repeat_delay):
    keyboard.the_repeat_rate = repeat_rate
    keyboard.the_repeat_delay = repeat_delay


def wl_keyboard_EVT_keymap(keyboard, keymap_type, keymap_fd, keymap_size):
    keyboard.the_keymap_type = keymap_type
    keyboard.the_keymap_fd = keymap_fd
    keyboard.the_keymap_size = keymap_size


def wl_keyboard_REQ_release(keyboard):
    keyboard.upstream.release()
    keyboard.seat.keyboards.remove(keyboard)


def wl_seat_REQ_get_keyboard(seat, keyboard):
    keyboard.upstream = upkey = seat.center.upstream.get_keyboard()
    upkey.downstream = keyboard

    keyboard.seat = seat.center
    seat.center.keyboards.append(keyboard)

    def after_roundtrip(cdisp, serial):
        keyboard.repeat_info(upkey.the_repeat_rate, upkey.the_repeat_delay)
        if upkey.the_keymap_fd is not None:
            keyboard.keymap(
                upkey.the_keymap_type, upkey.the_keymap_fd, upkey.the_keymap_size
            )

    upkey.current_surface_display = None
    upkey.the_repeat_rate = 24
    upkey.the_repeat_delay = 601
    upkey.the_keymap_type = None
    upkey.the_keymap_fd = None
    upkey.the_keymap_size = None
    upkey.dispatcher["repeat_info"] = wl_keyboard_EVT_repeat_info
    upkey.dispatcher["keymap"] = wl_keyboard_EVT_keymap
    upkey.dispatcher["key"] = wl_keyboard_EVT_key
    upkey.dispatcher["modifiers"] = wl_keyboard_EVT_modifiers
    upkey.dispatcher["enter"] = wl_keyboard_EVT_enter
    upkey.dispatcher["leave"] = wl_keyboard_EVT_leave
    keyboard.dispatcher["release"] = wl_keyboard_REQ_release

    subcallback = upkey.display.sync()
    subcallback.dispatcher["done"] = after_roundtrip

    # Q: what type of file descriptor do we send if no keymap?


def wl_seat_REQ_get_touch(seat, touch):
    raise NotImplementedError()


def wl_seat_EVT_name(seat, name):
    seat.the_name = name


def wl_seat_EVT_capabilities(seat, capabilities):
    # TODO: after the first time, directly forward the capability changes?
    # or just forward every time, and never use the sync() callback?
    if seat.the_capabilities is not None:
        raise NotImplementedError("capabilities change")
    seat.the_capabilities = capabilities


def xdg_wm_base_EVT_ping(xdg_wm_base, serial):
    dserial = xdg_wm_base.display.serial_downshift(xdg_wm_base.display, serial)
    # We broadcast to all bound copies of the global...
    for base in xdg_wm_base.center.downstreams:
        base.ping(dserial)


def xdg_wm_base_REQ_pong(xdg_wm_base, serial):
    updisp, userial = xdg_wm_base.display.serial_upshift(serial)
    xdg_wm_base.center.upstreams[updisp].pong(userial)


def wl_shm_EVT_format(wl_shm, fmt):
    wl_shm.formats.append(fmt)


def zwp_linux_buffer_params_v1_REQ_destroy(zwp_linux_buffer_params_v1):
    # todo: clear file descriptors as well...

    for cdisp in zwp_linux_buffer_params_v1.upstreams:
        zwp_linux_buffer_params_v1.upstreams[cdisp].destroy()
    zwp_linux_buffer_params_v1.base.params.remove(zwp_linux_buffer_params_v1)


def zwp_linux_buffer_params_v1_REQ_create(
    zwp_linux_buffer_params_v1, width, height, fmt, flags
):
    # Q: can we emulate this using the upstream's create_immed?
    raise NotImplementedError(
        "todo this should be straightforward with a sync cycle to check all failures"
    )


def zwp_linux_buffer_params_v1_REQ_add(
    zwp_linux_buffer_params_v1, fd, plane_idx, offset, stride, modifier_hi, modifier_lo
):
    zwp_linux_buffer_params_v1.fd_list.append(
        (fd, plane_idx, offset, stride, modifier_hi, modifier_lo)
    )

    for cdisp in zwp_linux_buffer_params_v1.upstreams:
        zwp_linux_buffer_params_v1.upstreams[cdisp].add(
            fd, plane_idx, offset, stride, modifier_hi, modifier_lo
        )


def zwp_linux_buffer_params_v1_REQ_create_immed(
    zwp_linux_buffer_params_v1, wl_buffer, width, height, fmt, flags
):
    wl_buffer.upstreams = {}

    wl_buffer.buftype = "dmabuf_immed"
    wl_buffer.create_args = (width, height, fmt, flags)

    _wl_buffer_make_common(wl_buffer)

    wl_buffer.params = zwp_linux_buffer_params_v1
    zwp_linux_buffer_params_v1.buffers.append(wl_buffer)

    for cdisp in zwp_linux_buffer_params_v1.upstreams:
        wl_buffer_ADD(wl_buffer, cdisp)


def zwp_linux_buffer_params_v1_EVT_failed(zwp_linux_buffer_params_v1):
    zwp_linux_buffer_params_v1.downstream.failed()


def zwp_linux_buffer_params_v1_DROP(zwp_linux_buffer_params_v1, cdisp):
    for buf in zwp_linux_buffer_params_v1.buffers:
        wl_buffer_DROP(buf, cdisp)
    del zwp_linux_buffer_params_v1.upstreams[cdisp]


def zwp_linux_dmabuf_params_v1_ADD(zwp_linux_buffer_params_v1, cdisp):
    if cdisp in zwp_linux_buffer_params_v1.upstreams:
        return

    zwp_linux_dmabuf_v1_ADD(zwp_linux_buffer_params_v1.base, cdisp)

    uparams = zwp_linux_buffer_params_v1.base.upstreams[cdisp].create_params()
    _link_updown(zwp_linux_buffer_params_v1, uparams)

    uparams.dispatcher["failed"] = zwp_linux_buffer_params_v1_EVT_failed

    for (
        fd,
        plane_idx,
        offset,
        stride,
        modifier_hi,
        modifier_lo,
    ) in zwp_linux_buffer_params_v1.fd_list:
        uparams.add(fd, plane_idx, offset, stride, modifier_hi, modifier_lo)

    for buf in zwp_linux_buffer_params_v1.buffers:
        wl_buffer_ADD(buf, cdisp)


def zwp_linux_dmabuf_v1_REQ_create_params(
    zwp_linux_dmabuf_v1, zwp_linux_buffer_params_v1
):
    zwp_linux_buffer_params_v1.dispatcher[
        "destroy"
    ] = zwp_linux_buffer_params_v1_REQ_destroy
    zwp_linux_buffer_params_v1.dispatcher["add"] = zwp_linux_buffer_params_v1_REQ_add
    zwp_linux_buffer_params_v1.dispatcher[
        "create_immed"
    ] = zwp_linux_buffer_params_v1_REQ_create_immed
    zwp_linux_buffer_params_v1.dispatcher[
        "create"
    ] = zwp_linux_buffer_params_v1_REQ_create

    zwp_linux_dmabuf_v1.center.params.append(zwp_linux_buffer_params_v1)
    zwp_linux_buffer_params_v1.base = zwp_linux_dmabuf_v1.center

    zwp_linux_buffer_params_v1.fd_list = []
    zwp_linux_buffer_params_v1.buffers = []

    zwp_linux_buffer_params_v1.upstreams = {}
    for cdisp in zwp_linux_dmabuf_v1.center.upstreams:
        zwp_linux_dmabuf_params_v1_ADD(zwp_linux_buffer_params_v1, cdisp)


def zwp_linux_dmabuf_v1_EVT_format(zwp_linux_dmabuf_v1, fmt):
    zwp_linux_dmabuf_v1.modifiers.append((fmt, 0, 0))


def zwp_linux_dmabuf_v1_EVT_modifier(
    zwp_linux_dmabuf_v1, fmt, modifier_hi, modifier_lo
):
    zwp_linux_dmabuf_v1.modifiers.append((fmt, modifier_hi, modifier_lo))


def zwp_linux_dmabuf_v1_DROP(zwp_linux_dmabuf_v1, cdisp):
    for params in zwp_linux_dmabuf_v1.params:
        zwp_linux_buffer_params_v1_DROP(params, cdisp)

    zwp_linux_dmabuf_v1.upstreams[cdisp].downstream = None
    del zwp_linux_dmabuf_v1.upstreams[cdisp]


def zwp_linux_dmabuf_v1_CLEAR(zwp_linux_dmabuf_v1, sdisp):
    params = [x for x in zwp_linux_dmabuf_v1.params if x.display == sdisp]
    for param in params:
        for buf in param.buffers:
            wl_buffer_REQ_destroy(buf)
        zwp_linux_buffer_params_v1_REQ_destroy(param)


def zwp_relative_pointer_v1_EVT_relative_motion(
    zwp_relative_pointer_v1, utime_hi, utime_lo, dx, dy, dx_unaccel, dy_unaccel
):
    zwp_relative_pointer_v1.downstream.relative_motion(
        utime_hi, utime_lo, dx, dy, dx_unaccel, dy_unaccel
    )


def zwp_relative_pointer_v1_REQ_destroy(zwp_relative_pointer_v1):
    zwp_relative_pointer_v1.upstream.destroy()
    zwp_relative_pointer_v1.manager.relative_pointers.remove(zwp_relative_pointer_v1)


def zwp_relative_pointer_v1_DROP(zwp_relative_pointer_v1):
    # do nothing -- killing the linked seat should invalidate everything
    # tied to it...
    pass


def zwp_relative_pointer_manager_v1_REQ_get_relative_pointer(
    manager, zwp_relative_pointer_v1, wl_pointer
):
    manager.center.relative_pointers.append(zwp_relative_pointer_v1)
    zwp_relative_pointer_v1.manager = manager.center

    uprp = manager.center.upstreams[wl_pointer.upstream.display].get_relative_pointer(
        wl_pointer.upstream
    )
    uprp.downstream = zwp_relative_pointer_v1
    zwp_relative_pointer_v1.upstream = uprp

    zwp_relative_pointer_v1.dispatcher["destroy"] = zwp_relative_pointer_v1_REQ_destroy
    uprp.dispatcher["relative_motion"] = zwp_relative_pointer_v1_EVT_relative_motion


def zwp_relative_pointer_manager_v1_ADD(manager, cdisp):
    if cdisp in manager.upstreams:
        return

    upglob = _create_static_global_upstream(manager, cdisp)

    # no autofill necessary, since relative pointers are linked to seats/compositors


def zwp_relative_pointer_manager_v1_DROP(manager, cdisp):
    for rp in manager.relative_pointers:
        if rp.upstream.display == cdisp:
            zwp_relative_pointer_v1_DROP(rp)
    manager.relative_pointers = [
        x for x in manager.relative_pointers if x.upstream.display != cdisp
    ]

    del manager.upstreams[cdisp]


def zwp_relative_pointer_manager_v1_CLEAR(manager, sdisp):
    rptr_list = [x for x in manager.relative_pointers if x.display == sdisp]
    for rel_pointer in rptr_list:
        zwp_relative_pointer_v1_REQ_destroy(rel_pointer)


def zwp_locked_pointer_v1_REQ_destroy(zwp_locked_pointer_v1):
    zwp_locked_pointer_v1.upstream.destroy()
    zwp_locked_pointer_v1.constraints.pointer_locks.remove(zwp_locked_pointer_v1)


def zwp_locked_pointer_v1_REQ_set_region(zwp_locked_pointer_v1, region):
    zwp_locked_pointer_v1.upstream.set_region(
        region.upstreams[zwp_locked_pointer_v1.upstream.display]
        if region is not None
        else None
    )


def zwp_locked_pointer_v1_REQ_set_cursor_position_hint(
    zwp_locked_pointer_v1, surface_x, surface_y
):
    zwp_locked_pointer_v1.upstream.set_region(surface_x, surface_y)


def zwp_locked_pointer_v1_EVT_locked(zwp_locked_pointer_v1):
    zwp_locked_pointer_v1.downstream.locked()


def zwp_locked_pointer_v1_EVT_unlocked(zwp_locked_pointer_v1):
    zwp_locked_pointer_v1.downstream.unlocked()


def zwp_pointer_constraints_v1_REQ_lock_pointer(
    constraints, zwp_locked_pointer_v1, surface, pointer, region, lifetime
):
    udisp = pointer.upstream.display

    wl_surface_ADD(surface, udisp)
    if region is not None:
        wl_region_ADD(region, udisp)

    constraints.center.pointer_locks.append(zwp_locked_pointer_v1)
    zwp_locked_pointer_v1.constraints = constraints.center

    uplock = constraints.center.upstreams[udisp].lock_pointer(
        surface.upstreams[udisp],
        pointer.upstream,
        region.upstreams[udisp] if region is not None else None,
        lifetime,
    )
    uplock.downstream = zwp_locked_pointer_v1
    zwp_locked_pointer_v1.upstream = uplock

    zwp_locked_pointer_v1.dispatcher["destroy"] = zwp_locked_pointer_v1_REQ_destroy
    zwp_locked_pointer_v1.dispatcher[
        "set_cursor_position_hint"
    ] = zwp_locked_pointer_v1_REQ_set_cursor_position_hint
    zwp_locked_pointer_v1.dispatcher[
        "set_region"
    ] = zwp_locked_pointer_v1_REQ_set_region
    uplock.dispatcher["locked"] = zwp_locked_pointer_v1_EVT_locked
    uplock.dispatcher["unlocked"] = zwp_locked_pointer_v1_EVT_unlocked


def zwp_confined_pointer_v1_REQ_destroy(zwp_confined_pointer_v1):
    zwp_confined_pointer_v1.upstream.destroy()
    zwp_confined_pointer_v1.constraints.pointer_confines.remove(zwp_confined_pointer_v1)


def zwp_confined_pointer_v1_REQ_set_region(zwp_confined_pointer_v1, region):
    zwp_confined_pointer_v1.upstream.set_region(
        region.upstreams[zwp_confined_pointer_v1.upstream.display]
        if region is not None
        else None
    )


def zwp_confined_pointer_v1_EVT_confined(zwp_confined_pointer_v1):
    zwp_confined_pointer_v1.downstream.confined()


def zwp_confined_pointer_v1_EVT_unconfined(zwp_confined_pointer_v1):
    zwp_confined_pointer_v1.downstream.unconfined()


def zwp_pointer_constraints_v1_REQ_confine_pointer(
    constraints, zwp_confined_pointer_v1, surface, pointer, region, lifetime
):
    udisp = pointer.upstream.display

    wl_surface_ADD(surface, udisp)
    if region is not None:
        wl_region_ADD(region, udisp)

    constraints.center.pointer_confines.append(zwp_confined_pointer_v1)
    zwp_confined_pointer_v1.constraints = constraints.center

    upconf = constraints.center.upstreams[udisp].confine_pointer(
        surface.upstreams[udisp],
        pointer.upstream,
        region.upstreams[udisp] if region is not None else None,
        lifetime,
    )
    upconf.downstream = zwp_confined_pointer_v1
    zwp_confined_pointer_v1.upstream = upconf

    zwp_confined_pointer_v1.dispatcher["destroy"] = zwp_confined_pointer_v1_REQ_destroy
    zwp_confined_pointer_v1.dispatcher[
        "set_region"
    ] = zwp_confined_pointer_v1_REQ_set_region
    upconf.dispatcher["confined"] = zwp_confined_pointer_v1_EVT_confined
    upconf.dispatcher["unconfined"] = zwp_confined_pointer_v1_EVT_unconfined


def zwp_pointer_constraints_v1_ADD(constraints, cdisp):
    if cdisp in constraints.upstreams:
        return

    upglob = _create_static_global_upstream(constraints, cdisp)


def zwp_pointer_constraints_v1_DROP(constraints, cdisp):
    constraints.pointer_locks = [
        x for x in constraints.pointer_locks if x.upstream.display != cdisp
    ]
    constraints.pointer_confines = [
        x for x in constraints.pointer_confines if x.upstream.display != cdisp
    ]
    del constraints.upstreams[cdisp]


def zwp_pointer_constraints_v1_CLEAR(constraints, sdisp):
    lock_list = [x for x in constraints.pointer_locks if x.display == sdisp]
    conf_list = [x for x in constraints.pointer_confines if x.display == sdisp]
    for lock in lock_list:
        zwp_locked_pointer_v1_REQ_destroy(lock)
    for conf in conf_list:
        zwp_confined_pointer_v1_REQ_destroy(conf)


def wl_data_source_REQ_offer(wl_data_source, mime_type):
    for cdisp in wl_data_source.upstreams:
        # TODO: dealing with late additions -- do we remember all of these?
        wl_data_source.upstreams[cdisp].offer(mime_type)


def wl_data_source_REQ_set_actions(wl_data_source, dnd_actions):
    for cdisp in wl_data_source.upstreams:
        wl_data_source.upstreams[cdisp].set_actions(dnd_actions)


def wl_data_source_REQ_destroy(wl_data_source):
    for cdisp in wl_data_source.upstreams:
        wl_data_source.upstreams[cdisp].destroy()

    wl_data_source.manager.sources.remove(wl_data_source)


def wl_data_source_EVT_target(wl_data_source, mime_type):
    wl_data_source.downstream.target(mime_type)


def wl_data_source_EVT_send(wl_data_source, mime_type, fd):
    wl_data_source.downstream.send(mime_type, fd)
    os.close(fd)


def wl_data_source_EVT_cancelled(wl_data_source):
    wl_data_source.downstream.cancelled()


def wl_data_source_EVT_dnd_drop_performed(wl_data_source):
    wl_data_source.downstream.dnd_drop_performed()


def wl_data_source_EVT_dnd_finished(wl_data_source):
    wl_data_source.downstream.dnd_finished()


def wl_data_source_EVT_action(wl_data_source, dnd_action):
    wl_data_source.downstream.action(dnd_action)


def wl_data_source_ADD(wl_data_source, cdisp):
    upsource = wl_data_source.manager.upstreams[cdisp].create_data_source()
    _link_updown(wl_data_source, upsource)

    upsource.dispatcher["target"] = wl_data_source_EVT_target
    upsource.dispatcher["send"] = wl_data_source_EVT_send
    upsource.dispatcher["cancelled"] = wl_data_source_EVT_cancelled
    upsource.dispatcher["dnd_drop_performed"] = wl_data_source_EVT_dnd_drop_performed
    upsource.dispatcher["dnd_finished"] = wl_data_source_EVT_dnd_finished
    upsource.dispatcher["action"] = wl_data_source_EVT_action
    # todo: replay info


def wl_data_device_manager_REQ_create_data_source(manager, wl_data_source):
    manager.center.sources.append(wl_data_source)
    wl_data_source.manager = manager.center

    # todo: remember and replay offer info

    wl_data_source.dispatcher["offer"] = wl_data_source_REQ_offer
    wl_data_source.dispatcher["set_actions"] = wl_data_source_REQ_set_actions
    wl_data_source.dispatcher["destroy"] = wl_data_source_REQ_destroy

    wl_data_source.upstreams = {}
    for cdisp in manager.center.upstreams:
        wl_data_source_ADD(wl_data_source, cdisp)


def wl_data_device_REQ_start_drag(wl_data_device, source, origin, icon, serial):
    updisp, userial = wl_data_device.display.serial_upshift(serial)
    if updisp != wl_data_device.upstream.display:
        print(
            "wl_data_device.set_selection called with bad serial",
            updisp,
            wl_data_device.upstream.display,
        )
        return
    wl_data_device.upstream.set_selection(
        source.upstreams[updisp] if source is not None else None,
        origin.upstreams[updisp],
        icon.upstreams[updisp] if icon is not None else None,
        userial,
    )


def wl_data_device_REQ_set_selection(wl_data_device, source, serial):
    updisp, userial = wl_data_device.display.serial_upshift(serial)
    if updisp != wl_data_device.upstream.display:
        print(
            "wl_data_device.set_selection called with bad serial",
            updisp,
            wl_data_device.upstream.display,
        )
        return
    wl_data_device.upstream.set_selection(
        source.upstreams[updisp] if source is not None else None, userial
    )


def wl_data_device_REQ_release(wl_data_device):
    wl_data_device.upstream.release()
    wl_data_device.manager.devices.remove(wl_data_device)


def wl_data_offer_EVT_offer(wl_data_offer, mime_type):
    wl_data_offer.downstream.offer(mime_type)


def wl_data_offer_EVT_source_actions(wl_data_offer, source_actions):
    wl_data_offer.downstream.source_actions(source_actions)


def wl_data_offer_EVT_action(wl_data_offer, dnd_action):
    wl_data_offer.downstream.action(dnd_action)


def wl_data_offer_REQ_accept(wl_data_offer, serial, mime_type):
    updisp, userial = wl_data_offer.display.serial_upshift(serial)
    if updisp != wl_data_offer.upstream.display:
        print(
            "Tried to accept offer with a serial from another compositor",
            updisp,
            wl_data_offer.upstream.display,
        )
        return

    wl_data_offer.upstream.accept(userial, mime_type)


def wl_data_offer_REQ_receive(wl_data_offer, mime_type, fd):
    wl_data_offer.upstream.receive(mime_type, fd)
    os.close(fd)


def wl_data_offer_REQ_destroy(wl_data_offer):
    wl_data_offer.upstream.destroy()
    wl_data_offer.device.offers.remove(wl_data_offer)


def wl_data_offer_REQ_finish(wl_data_offer):
    wl_data_offer.upstream.finish()


def wl_data_offer_REQ_set_actions(wl_data_offer, dnd_actions, preferred_action):
    wl_data_offer.upstream.set_actions(dnd_actions, preferred_action)


def wl_data_device_EVT_data_offer(wl_data_device, wl_data_offer):
    downoffer = wl_data_device.downstream.data_offer()
    downoffer.upstream = wl_data_offer
    wl_data_offer.downstream = downoffer

    wl_data_device.downstream.offers.append(downoffer)
    downoffer.device = wl_data_device.downstream

    wl_data_offer.dispatcher["offer"] = wl_data_offer_EVT_offer
    wl_data_offer.dispatcher["source_actions"] = wl_data_offer_EVT_source_actions
    wl_data_offer.dispatcher["action"] = wl_data_offer_EVT_action

    downoffer.dispatcher["accept"] = wl_data_offer_REQ_accept
    downoffer.dispatcher["receive"] = wl_data_offer_REQ_receive
    downoffer.dispatcher["destroy"] = wl_data_offer_REQ_destroy
    downoffer.dispatcher["finish"] = wl_data_offer_REQ_finish
    downoffer.dispatcher["set_actions"] = wl_data_offer_REQ_set_actions


def wl_data_device_EVT_enter(wl_data_device, serial, surface, x, y, _id):
    dserial = wl_data_device.display.serial_downshift(wl_data_device.display, serial)
    did = _id.downstream if _id is not None else None
    wl_data_device.downstream.enter(serial, surface.downstream, x, y, did)


def wl_data_device_EVT_leave(wl_data_device):
    wl_data_device.downstream.leave()


def wl_data_device_EVT_motion(wl_data_device, time, x, y):
    wl_data_device.downstream.motion(time, x, y)


def wl_data_device_EVT_drop(wl_data_device):
    wl_data_device.downstream.drop()


def wl_data_device_EVT_selection(wl_data_device, _id):
    wl_data_device.downstream.selection(_id.downstream if _id is not None else None)


def wl_data_device_DROP(wl_data_device):
    pass


def wl_data_device_manager_REQ_get_data_device(manager, wl_data_device, seat):
    manager.center.devices.append(wl_data_device)
    wl_data_device.manager = manager.center
    wl_data_device.seat = seat

    wl_data_device.offers = []

    wl_data_device.dispatcher["start_drag"] = wl_data_device_REQ_start_drag
    wl_data_device.dispatcher["set_selection"] = wl_data_device_REQ_set_selection
    wl_data_device.dispatcher["release"] = wl_data_device_REQ_release

    cdisp = seat.center.upstream.display
    upd = wl_data_device.manager.upstreams[cdisp].get_data_device(seat.center.upstream)
    wl_data_device.upstream = upd
    upd.downstream = wl_data_device

    upd.dispatcher["data_offer"] = wl_data_device_EVT_data_offer
    upd.dispatcher["enter"] = wl_data_device_EVT_enter
    upd.dispatcher["leave"] = wl_data_device_EVT_leave
    upd.dispatcher["motion"] = wl_data_device_EVT_motion
    upd.dispatcher["drop"] = wl_data_device_EVT_drop
    upd.dispatcher["selection"] = wl_data_device_EVT_selection


def wl_data_device_manager_ADD(manager, cdisp):
    if cdisp in manager.upstreams:
        return

    reg = manager.registry
    upglob = _create_static_global_upstream(manager, cdisp)

    for source in manager.sources:
        wl_data_source_ADD(source, cdisp)
    # data devices are per-seat


def wl_data_device_manager_DROP(manager, cdisp):
    for source in manager.sources:
        wl_data_source_DROP(source, cdisp)
    for device in manager.devices:
        if device.upstream.display == cdisp:
            wl_data_device_DROP(device)
    manager.devices = [x for x in manager.devices if x.upstream.display != cdisp]
    del manager.upstreams[cdisp]


def wl_data_device_manager_CLEAR(manager, sdisp):
    source_list = [x for x in manager.sources if x.display == sdisp]
    device_list = [x for x in manager.devices if x.display == sdisp]
    for source in source_list:
        wl_data_source_REQ_destroy(source)
    for device in device_list:
        for offer in device.offers:
            wl_data_offer_REQ_destroy(offer)
        wl_data_device_REQ_release(device)


def zwp_primary_selection_source_v1_EVT_send(source, mime_type, fd):
    source.downstream.send(mime_type, fd)
    os.close(fd)


def zwp_primary_selection_source_v1_EVT_cancelled(source):
    source.downstream.cancelled()


def zwp_primary_selection_source_v1_REQ_offer(source, mime_type):
    for cdisp in source.upstreams:
        # todo: and late additions?
        source.upstreams[cdisp].offer(mime_type)


def zwp_primary_selection_source_v1_REQ_destroy(source):
    for cdisp in source.upstreams:
        source.upstreams[cdisp].destroy()
    source.manager.sources.remove(source)


def zwp_primary_selection_source_v1_ADD(source, cdisp):
    upsource = source.manager.upstreams[cdisp].create_source()
    _link_updown(source, upsource)

    upsource.dispatcher["send"] = zwp_primary_selection_source_v1_EVT_send
    upsource.dispatcher["cancelled"] = zwp_primary_selection_source_v1_EVT_cancelled


def zwp_primary_selection_device_manager_v1_REQ_create_source(
    manager, zwp_primary_selection_source_v1
):
    manager.center.sources.append(zwp_primary_selection_source_v1)
    zwp_primary_selection_source_v1.manager = manager.center

    # todo: remember and replay offer info

    zwp_primary_selection_source_v1.dispatcher[
        "offer"
    ] = zwp_primary_selection_source_v1_REQ_offer
    zwp_primary_selection_source_v1.dispatcher[
        "destroy"
    ] = zwp_primary_selection_source_v1_REQ_destroy

    zwp_primary_selection_source_v1.upstreams = {}
    for cdisp in manager.center.upstreams:
        zwp_primary_selection_source_v1_ADD(zwp_primary_selection_source_v1, cdisp)


def zwp_primary_selection_device_v1_REQ_set_selection(device, source, serial):
    updisp, userial = device.display.serial_upshift(serial)
    if updisp != device.upstream.display:
        print(
            "zwp_primary_selection_device_v1.set_selection called with bad serial",
            updisp,
            device.upstream.display,
        )
        return
    device.upstream.set_selection(
        source.upstreams[updisp] if source is not None else None, userial
    )


def zwp_primary_selection_device_v1_REQ_destroy(device):
    device.upstream.destroy()
    device.manager.devices.remove(device)


def zwp_primary_selection_offer_v1_EVT_offer(offer, mime_type):
    offer.downstream.offer(mime_type)


def zwp_primary_selection_offer_v1_REQ_receive(offer, mime_type, fd):
    offer.upstream.receive(mime_type, fd)
    os.close(fd)


def zwp_primary_selection_offer_v1_REQ_destroy(offer):
    offer.upstream.destroy()
    offer.device.offers.remove(offer)


def zwp_primary_selection_device_v1_EVT_data_offer(
    device, zwp_primary_selection_offer_v1
):
    downoffer = device.downstream.data_offer()
    downoffer.upstream = zwp_primary_selection_offer_v1
    zwp_primary_selection_offer_v1.downstream = downoffer

    device.downstream.offers.append(downoffer)
    downoffer.device = device.downstream

    zwp_primary_selection_offer_v1.dispatcher[
        "offer"
    ] = zwp_primary_selection_offer_v1_EVT_offer

    downoffer.dispatcher["receive"] = zwp_primary_selection_offer_v1_REQ_receive
    downoffer.dispatcher["destroy"] = zwp_primary_selection_offer_v1_REQ_destroy


def zwp_primary_selection_device_v1_EVT_selection(device, _id):
    device.downstream.selection(_id.downstream if _id is not None else None)


def zwp_primary_selection_device_manager_v1_REQ_get_device(
    manager, zwp_primary_selection_device_v1, seat
):
    manager.center.devices.append(zwp_primary_selection_device_v1)
    zwp_primary_selection_device_v1.manager = manager.center
    zwp_primary_selection_device_v1.seat = seat

    zwp_primary_selection_device_v1.offers = []

    zwp_primary_selection_device_v1.dispatcher[
        "set_selection"
    ] = zwp_primary_selection_device_v1_REQ_set_selection
    zwp_primary_selection_device_v1.dispatcher[
        "destroy"
    ] = zwp_primary_selection_device_v1_REQ_destroy

    cdisp = seat.center.upstream.display
    upd = manager.center.upstreams[cdisp].get_device(seat.center.upstream)
    zwp_primary_selection_device_v1.upstream = upd
    upd.downstream = zwp_primary_selection_device_v1

    upd.dispatcher["data_offer"] = zwp_primary_selection_device_v1_EVT_data_offer
    upd.dispatcher["selection"] = zwp_primary_selection_device_v1_EVT_selection


def zwp_primary_selection_device_manager_v1_ADD(manager, cdisp):
    if cdisp in manager.upstreams:
        return

    reg = manager.registry
    upglob = _create_static_global_upstream(manager, cdisp)

    for source in manager.sources:
        zwp_primary_selection_source_v1_ADD(source, cdisp)
    # devices are per-seat


def zwp_primary_selection_device_manager_v1_DROP(manager, cdisp):
    for source in manager.sources:
        zwp_primary_selection_source_v1_DROP(source, cdisp)
    manager.devices = [x for x in manager.devices if x.upstream.display != cdisp]
    del manager.upstreams[cdisp]


def zwp_primary_selection_device_manager_v1_CLEAR(manager, sdisp):
    source_list = [x for x in manager.sources if x.display == sdisp]
    device_list = [x for x in manager.devices if x.display == sdisp]
    for source in source_list:
        zwp_primary_selection_source_v1_REQ_destroy(source)
    for device in device_list:
        for offer in device.offers:
            zwp_primary_selection_offer_v1_REQ_destroy(offer)
        zwp_primary_selection_device_v1_REQ_destroy(device)


def wl_output_REQ_release(wl_output):
    # todo: find out what to do here. releasing the upstream
    # could break the other downstreams; perhaps set a flag to
    # filter out items as soft-deleted?
    pass


def wl_output_EVT_done(wl_output):
    for downput in wl_output.center.downstreams:
        if wl_output.the_geometry is not None:
            downput.geometry(*wl_output.the_geometry)
        if wl_output.the_mode is not None:
            downput.mode(*wl_output.the_mode)
        if wl_output.the_scale is not None:
            downput.scale(wl_output.the_scale)
        downput.done()
    wl_output.info_ready = True


def wl_output_EVT_geometry(
    wl_output, x, y, physical_width, physical_height, subpixel, make, model, transform
):
    physical_fudge = 1.1
    wl_output.the_geometry = (
        x,
        y,
        int(physical_width * physical_fudge),
        int(physical_height * physical_fudge),
        subpixel,
        make,
        model,
        transform,
    )


def wl_output_EVT_mode(wl_output, flags, width, height, refresh):
    wl_output.the_mode = (flags, width, height, refresh)


def wl_output_EVT_scale(wl_output, factor):
    wl_output.the_scale = factor


def _registry_globlist_version_for_name(registry, globname):
    matching = [
        (uname, uversion)
        for (uname, uinterface, uversion) in registry.global_list
        if uinterface == globname
    ]
    if len(matching):
        return matching[0]
    else:
        raise KeyError


def wl_shm_ADD(wl_shm, cdisp):
    if cdisp in wl_shm.upstreams:
        return

    def verify_formats_available(callback, data):
        upshm = wl_shm.upstreams[callback.display]
        if set(wl_shm.basic_format_list) - set(upshm.formats):
            raise Exception("The upstream did not support all the basic shm formats")

    upglob = _create_static_global_upstream(wl_shm, cdisp)
    upglob.dispatcher["format"] = wl_shm_EVT_format

    upglob.formats = []
    upglob.formats_synced = False

    # synchronize to force receiving of shm types
    subcallback = cdisp.sync()
    subcallback.dispatcher["done"] = verify_formats_available

    for shm_pool in wl_shm.pools:
        wl_shm_pool_ADD(shm_pool, cdisp)


def zwp_linux_dmabuf_v1_ADD(zwp_linux_dmabuf_v1, cdisp):
    if cdisp in zwp_linux_dmabuf_v1.upstreams:
        return

    def verify_modifiers_available(callback, data):
        updmabuf = zwp_linux_dmabuf_v1.upstreams[callback.display]
        if set(zwp_linux_dmabuf_v1.basic_modifier_list) - set(updmabuf.modifiers):
            raise Exception(
                "The upstream did not support all the basic dmabuf modifiers"
            )

    upglob = _create_static_global_upstream(zwp_linux_dmabuf_v1, cdisp)
    upglob.dispatcher["modifier"] = zwp_linux_dmabuf_v1_EVT_modifier
    upglob.dispatcher["format"] = zwp_linux_dmabuf_v1_EVT_format

    upglob.modifiers = []
    upglob.modifiers_synced = False

    # synchronize to force receiving of shm types
    subcallback = cdisp.sync()
    subcallback.dispatcher["done"] = verify_modifiers_available

    for params in zwp_linux_dmabuf_v1.params:
        zwp_linux_dmabuf_params_v1_ADD(params, cdisp)


def wl_drm_EVT_format(wl_drm, fmt):
    wl_drm.formats.append(fmt)


def wl_drm_ADD(wl_drm, cdisp):
    if cdisp in wl_drm.upstreams:
        return

    upglob = _create_static_global_upstream(wl_drm, cdisp)
    upglob.dispatcher["format"] = wl_drm_EVT_format
    upglob.formats = []

    def verify_formats_available(callback, data):
        updrm = wl_drm.upstreams[callback.display]
        if set(wl_drm.format_list) - set(updrm.formats):
            raise Exception("The upstream did not support all the basic wl_drm formats")

    subcallback = cdisp.sync()
    subcallback.dispatcher["done"] = verify_formats_available

    # todo: bind, and verify upstream supports everything we advertised,
    # either via wl_drm, or zwp_linux_dmabuf
    pass


def wl_drm_DROP(wl_drm, cdisp):
    if cdisp not in wl_drm.upstreams:
        return
    wl_drm.upstreams[cdisp].downstream = None
    del wl_drm.upstreams[cdisp]


def wl_compositor_ADD(wl_compositor, cdisp):
    if cdisp in wl_compositor.upstreams:
        return

    upglob = _create_static_global_upstream(wl_compositor, cdisp)

    # Make regions first, as surfaces use regions, but not vice versa
    for region in wl_compositor.regions:
        wl_region_ADD(region, cdisp)
    for surface in wl_compositor.surfaces:
        wl_surface_ADD(surface, cdisp)


def wl_subcompositor_ADD(wl_subcompositor, cdisp):
    if cdisp in wl_subcompositor.upstreams:
        return

    upglob = _create_static_global_upstream(wl_subcompositor, cdisp)

    # TODO: if any subsurfaces have been created, make matching subsurfaces!


def zxdg_decoration_manager_v1_ADD(zxdg_decoration_manager_v1, cdisp):
    if cdisp in zxdg_decoration_manager_v1.upstreams:
        return

    upglob = _create_static_global_upstream(zxdg_decoration_manager_v1, cdisp)
    if upglob is None:
        print(
            "Not able to create xdg-decoration global; will be ignoring all requests to this"
        )
        return

    for deco in zxdg_decoration_manager_v1.decorations:
        zxdg_toplevel_decoration_v1_ADD(deco, cdisp)


def xdg_wm_base_ADD(xdg_wm_base, cdisp):
    if cdisp in xdg_wm_base.upstreams:
        return

    upglob = _create_static_global_upstream(xdg_wm_base, cdisp)
    upglob.dispatcher["ping"] = xdg_wm_base_EVT_ping

    for surface in xdg_wm_base.surfaces:
        xdg_surface_ADD(surface, cdisp)
    for positioner in xdg_wm_base.positioners:
        xdg_positioner_ADD(surface, cdisp)


def _registry_init_main(glob):
    mainreg = glob.registry
    intf_list = mainreg.interfaces

    if glob.global_name == "xdg_wm_base":
        glob.surfaces = []
        glob.positioners = []

        for cdisp in mainreg.upstreams:
            if mainreg.upstreams[cdisp].init_fill:
                xdg_wm_base_ADD(glob, cdisp)

    elif glob.global_name == "wl_shm":
        glob.upstreams = {}
        glob.pools = []

        all_fmts = intf_list["wl_shm"].enums["format"]
        glob.basic_format_list = [0, 1]  # xrgb,argb 8888

        for cdisp in mainreg.upstreams:
            if mainreg.upstreams[cdisp].init_fill:
                wl_shm_ADD(glob, cdisp)

    elif glob.global_name == "zwp_linux_dmabuf_v1":
        glob.params = []
        # We provide a "core" set of modifiers, which we require that all upstreams support
        glob.basic_modifier_list = [
            (1211384385, 0, 0),
            (1211384408, 0, 0),
            (808669761, 0, 0),
            (808669784, 0, 0),
            (808665665, 0, 0),
            (875713089, 0, 0),
            (875708993, 0, 0),
            (875713112, 0, 0),
            (875709016, 0, 0),
            (892424769, 0, 0),
            (909199186, 0, 0),
            (538982482, 0, 0),
            (540422482, 0, 0),
            (943215175, 0, 0),
            (842224199, 0, 0),
            (961959257, 0, 0),
            (825316697, 0, 0),
            (842093913, 0, 0),
            (909202777, 0, 0),
            (875713881, 0, 0),
            (961893977, 0, 0),
            (825316953, 0, 0),
            (842094169, 0, 0),
            (909203033, 0, 0),
            (875714137, 0, 0),
            (842094158, 0, 0),
            (808530000, 0, 0),
            (842084432, 0, 0),
            (909193296, 0, 0),
            (909203022, 0, 0),
            (1448433985, 0, 0),
            (1448434008, 0, 0),
            (1448695129, 0, 0),
            (1498831189, 0, 0),
        ]

        for cdisp in mainreg.upstreams:
            if mainreg.upstreams[cdisp].init_fill:
                zwp_linux_dmabuf_v1_ADD(glob, cdisp)

    elif glob.global_name == "wl_compositor":
        glob.surfaces = []
        glob.regions = []

        for cdisp in mainreg.upstreams:
            if mainreg.upstreams[cdisp].init_fill:
                wl_compositor_ADD(glob, cdisp)

    elif glob.global_name == "wl_subcompositor":
        for cdisp in mainreg.upstreams:
            if mainreg.upstreams[cdisp].init_fill:
                wl_subcompositor_ADD(glob, cdisp)

    elif glob.global_name == "zxdg_decoration_manager_v1":
        glob.decorations = []
        for cdisp in mainreg.upstreams:
            if mainreg.upstreams[cdisp].init_fill:
                zxdg_decoration_manager_v1_ADD(glob, cdisp)

    elif glob.global_name == "zwp_relative_pointer_manager_v1":
        glob.relative_pointers = []
        for cdisp in mainreg.upstreams:
            if mainreg.upstreams[cdisp].init_fill:
                zwp_relative_pointer_manager_v1_ADD(glob, cdisp)

    elif glob.global_name == "zwp_pointer_constraints_v1":
        glob.pointer_locks = []
        glob.pointer_confines = []
        for cdisp in mainreg.upstreams:
            if mainreg.upstreams[cdisp].init_fill:
                zwp_pointer_constraints_v1_ADD(glob, cdisp)

    elif glob.global_name == "zwp_primary_selection_device_manager_v1":
        glob.sources = []
        glob.devices = []
        for cdisp in mainreg.upstreams:
            if mainreg.upstreams[cdisp].init_fill:
                zwp_primary_selection_device_manager_v1_ADD(glob, cdisp)

    elif glob.global_name == "wl_data_device_manager":
        glob.sources = []
        glob.devices = []
        for cdisp in mainreg.upstreams:
            if mainreg.upstreams[cdisp].init_fill:
                wl_data_device_manager_ADD(glob, cdisp)

    elif glob.global_name == "wl_drm":
        glob.drm_path = "/dev/dri/renderD128"  # render node, not main card
        glob.format_list = [
            808669761,
            808669784,
            808665665,
            875713089,
            875713112,
            909199186,
            961959257,
            825316697,
            842093913,
            909202777,
            875713881,
            842094158,
            909203022,
            1448695129,
        ]

        # todo: implement the 'prime buffer create' functions?
        for cdisp in mainreg.upstreams:
            if mainreg.upstreams[cdisp].init_fill:
                wl_drm_ADD(glob, cdisp)

    elif glob.global_name == "wl_seat":
        # Add sole upstream
        cdisp, cname, cversion = glob.upstream_info
        upseat = mainreg.upstreams[cdisp].bind(
            cname, cdisp.interfaces["wl_seat"], cversion
        )

        glob.upstream = upseat
        upseat.center = glob

        glob.keyboards = []
        glob.pointers = []

        upseat.the_name = None
        upseat.the_capabilities = None
        upseat.dispatcher["name"] = wl_seat_EVT_name
        upseat.dispatcher["capabilities"] = wl_seat_EVT_capabilities
        upseat.info_ready = False

        def after_roundtrip(cdisp, serial):
            upseat.info_ready = True
            name = upseat.the_name if upseat.the_name is not None else "unknown"
            if upseat.the_capabilities is None:
                raise Exception("initial capabilities should have been set for", cdisp)

            for intf in glob.downstreams:
                if intf.version >= 2:
                    # seat name only supported for version >= 2
                    intf.name(name + "_" + str(cdisp.display.prefix))
                intf.capabilities(upseat.the_capabilities)

        subcallback = cdisp.sync()
        subcallback.dispatcher["done"] = after_roundtrip

    elif glob.global_name == "wl_output":
        cdisp, cname, cversion = glob.upstream_info
        uput = mainreg.upstreams[cdisp].bind(
            cname, cdisp.interfaces["wl_output"], cversion
        )
        glob.upstream = uput
        uput.center = glob

        uput.dispatcher["done"] = wl_output_EVT_done
        uput.dispatcher["geometry"] = wl_output_EVT_geometry
        uput.dispatcher["mode"] = wl_output_EVT_mode
        uput.dispatcher["scale"] = wl_output_EVT_scale

        # Despite the "done" event, we use the same delay/retransmit
        # routine as wl_seat, because downstream programs could bind halfway through
        uput.the_geometry = None
        uput.the_mode = None
        uput.the_scale = None
        uput.info_ready = False

    else:
        raise NotImplementedError(glob.global_name)


def _registry_setup_downstream(glob, intf):
    glob.downstreams.append(intf)
    intf.center = glob

    if glob.global_name == "xdg_wm_base":
        intf.dispatcher["get_xdg_surface"] = xdg_wm_base_REQ_get_xdg_surface
        intf.dispatcher["create_positioner"] = xdg_wm_base_REQ_create_positioner
        intf.dispatcher["pong"] = xdg_wm_base_REQ_pong

    elif glob.global_name == "wl_shm":
        for fmt in sorted(glob.basic_format_list):
            intf.format(fmt)
        intf.dispatcher["create_pool"] = wl_shm_REQ_create_pool

    elif glob.global_name == "zwp_linux_dmabuf_v1":
        intf.dispatcher["create_params"] = zwp_linux_dmabuf_v1_REQ_create_params
        for fmt, mod_hi, mod_lo in sorted(glob.basic_modifier_list):
            intf.modifier(fmt, mod_hi, mod_lo)

    elif glob.global_name == "wl_compositor":
        intf.dispatcher["create_surface"] = wl_compositor_REQ_create_surface
        intf.dispatcher["create_region"] = wl_compositor_REQ_create_region

    elif glob.global_name == "wl_subcompositor":
        intf.dispatcher["get_subsurface"] = wl_subcompositor_REQ_get_subsurface

    elif glob.global_name == "zxdg_decoration_manager_v1":
        intf.dispatcher[
            "get_toplevel_decoration"
        ] = zxdg_decoration_manager_v1_REQ_get_toplevel_decoration

    elif glob.global_name == "zwp_relative_pointer_manager_v1":
        intf.dispatcher[
            "get_relative_pointer"
        ] = zwp_relative_pointer_manager_v1_REQ_get_relative_pointer

    elif glob.global_name == "zwp_pointer_constraints_v1":
        intf.dispatcher["lock_pointer"] = zwp_pointer_constraints_v1_REQ_lock_pointer
        intf.dispatcher[
            "confine_pointer"
        ] = zwp_pointer_constraints_v1_REQ_confine_pointer

    elif glob.global_name == "zwp_primary_selection_device_manager_v1":
        intf.dispatcher[
            "create_source"
        ] = zwp_primary_selection_device_manager_v1_REQ_create_source
        intf.dispatcher[
            "get_device"
        ] = zwp_primary_selection_device_manager_v1_REQ_get_device

    elif glob.global_name == "wl_data_device_manager":
        intf.dispatcher[
            "create_data_source"
        ] = wl_data_device_manager_REQ_create_data_source
        intf.dispatcher["get_data_device"] = wl_data_device_manager_REQ_get_data_device

    elif glob.global_name == "wl_drm":
        intf.dispatcher["authenticate"] = NotImplemented

        intf.device(glob.drm_path)
        for fmt in glob.format_list:
            intf.format(fmt)
        intf.capabilities(1)

    elif glob.global_name == "wl_seat":
        intf.dispatcher["get_keyboard"] = wl_seat_REQ_get_keyboard
        intf.dispatcher["get_pointer"] = wl_seat_REQ_get_pointer
        intf.dispatcher["get_touch"] = wl_seat_REQ_get_touch
        # ignore 'release' request

        if glob.upstream.info_ready:
            # otherwise, info will be provided by the callback
            if intf.version >= 2:
                # seat name only supported for version >= 2
                intf.name(
                    glob.upstream.the_name + "_" + str(glob.upstream.display.prefix)
                )
            intf.capabilities(glob.upstream.the_capabilities)

    elif glob.global_name == "wl_output":
        intf.dispatcher["release"] = wl_output_REQ_release

        uput = glob.upstream
        if uput.info_ready:
            if uput.the_geometry is not None:
                intf.geometry(*uput.the_geometry)
            if uput.the_mode is not None:
                intf.mode(*uput.the_mode)
            if uput.the_scale is not None:
                intf.scale(uput.the_scale)
            intf.done()
    else:
        raise NotImplementedError(glob.global_name)


def wl_registry_REQ_bind(registry, name, intf):
    mainreg = registry.upstream
    intf_list = registry.display.interfaces

    # Verify that the name (integer) being bound matches the listed type
    globdex = name - 1
    globname, upstream_info = mainreg.advertised_globals[globdex]
    assert intf.interface == intf_list[globname]

    for glob in mainreg.created_globals:
        if glob.reg_adv_name == name:
            assert glob.global_name == intf.interface.name
            _registry_setup_downstream(glob, intf)
            return

    type_name = intf.interface.name
    if type_name in {"wl_seat", "wl_output"}:
        glob = DynamicGlobal(type_name, name)
    else:
        glob = StaticGlobal(type_name, name)
    # Record all globals created, in case we need to expand their upstreams
    mainreg.created_globals.append(glob)
    glob.registry = mainreg
    glob.upstream_info = upstream_info

    # The first time we create a global, we need to set specific variables
    _registry_init_main(glob)
    _registry_setup_downstream(glob, intf)


def wl_registry_EVT_global(registry, name, interface, version):
    registry.global_list.append((name, interface, version))


def _wl_registry_globals_done(wl_registry):
    wl_registry.init_fill = True
    # Validate that the necessary globals were provided

    glob_names = set(x[1] for x in wl_registry.global_list)
    mainreg = wl_registry.downstream

    for glob in mainreg.mandatory_globals:
        if glob not in glob_names:
            raise Exception("Upstream does not support global", glob)

    # Advertise the new variable globals to everything that has seen
    # them (which may be nobody if the client has not yet asked for a
    # registry)
    globevts = []
    for downreg in mainreg.downstreams:
        globevts.append(getattr(downreg, "global"))

    for name, interface, version in wl_registry.global_list:
        if interface == "wl_seat" or interface == "wl_output":
            mainreg.advertised_globals.append(
                (interface, (wl_registry.display, name, version))
            )
            for globevt in globevts:
                globevt(
                    len(mainreg.advertised_globals),
                    interface,
                    mainreg.supported_global_versions[interface],
                )

    # Backfill all the existing globals that we already promised to create
    disp = wl_registry.display
    for globobj in mainreg.created_globals:
        globname = globobj.global_name
        if globname in ("wl_seat", "wl_output"):
            # These are direct forwarding, not distributive type
            pass
        elif globname == "wl_shm":
            wl_shm_ADD(globobj, disp)
        elif globname == "wl_compositor":
            wl_compositor_ADD(globobj, disp)
        elif globname == "wl_subcompositor":
            wl_subcompositor_ADD(globobj, disp)
        elif globname == "zwp_linux_dmabuf_v1":
            zwp_linux_dmabuf_v1_ADD(globobj, disp)
        elif globname == "wl_drm":
            wl_drm_ADD(globobj, disp)
        elif globname == "xdg_wm_base":
            xdg_wm_base_ADD(globobj, disp)
        elif globname == "zxdg_decoration_manager_v1":
            zxdg_decoration_manager_v1_ADD(globobj, disp)
        elif globname == "zwp_relative_pointer_manager_v1":
            zwp_relative_pointer_manager_v1_ADD(globobj, disp)
        elif globname == "zwp_pointer_constraints_v1":
            zwp_pointer_constraints_v1_ADD(globobj, disp)
        elif globname == "zwp_primary_selection_device_manager_v1":
            zwp_primary_selection_device_manager_v1_ADD(globobj, disp)
        elif globname == "wl_data_device_manager":
            wl_data_device_manager_ADD(globobj, disp)
        else:
            raise Exception("Unhandled backfill!", globname)


def wl_registry_ADD(wl_registry, cdisp):
    if cdisp in wl_registry.upstreams:
        return

    subreg = cdisp.get_registry()
    _link_updown(wl_registry, subreg)

    subreg.dispatcher["global"] = wl_registry_EVT_global
    subreg.init_fill = False
    subreg.global_list = []

    def _reg_globals_done(callback, data):
        subreg = wl_registry.upstreams[callback.display]
        _wl_registry_globals_done(subreg)

    # After upstream has sent all globals, check that the upstream meets standards,
    # and then implement whatever globals have been bound by downstream
    subcallback = cdisp.sync()
    subcallback.dispatcher["done"] = _reg_globals_done


def wl_display_REQ_get_registry(display, registry):
    mainreg = display.upstream.registry
    mainreg.downstreams.append(registry)
    registry.upstream = mainreg

    globevt = getattr(registry, "global")
    registry.dispatcher["bind"] = wl_registry_REQ_bind

    for i, (glob, upstream_info) in enumerate(mainreg.advertised_globals):
        globevt(i + 1, glob, mainreg.supported_global_versions[glob])


def wl_seat_DROP(wl_seat):
    # First, invalidate keyboard and pointer, to increase chance clients handle this right
    for downseat in wl_seat.downstreams:
        downseat.capabilities(0)

    # q: what about cursor surfaces linked to the seat pointer?
    for downreg in wl_seat.registry.downstreams:
        downreg.global_remove(wl_seat.reg_adv_name)
    wl_seat.registry.created_globals.remove(wl_seat)


def wl_seat_CLEAR(wl_seat, sdisp):
    kbd_list = [x for x in wl_seat.keyboards if x.display == sdisp]
    for keyboard in kbd_list:
        wl_keyboard_REQ_release(keyboard)
    ptr_list = [x for x in wl_seat.pointers if x.display == sdisp]
    for pointer in ptr_list:
        wl_pointer_REQ_release(pointer)


def wl_output_DROP(wl_output):
    for downreg in wl_output.registry.downstreams:
        downreg.global_remove(wl_output.reg_adv_name)
    wl_output.registry.created_globals.remove(wl_output)


def wl_output_CLEAR(wl_output, sdisp):
    # no dependent objects...
    pass


def wl_registry_DROP(wl_registry, cdisp):
    for globobj in wl_registry.created_globals:
        globname = globobj.global_name
        if globname == "wl_seat":
            if globobj.upstream.display == cdisp:
                wl_seat_DROP(globobj)
        elif globname == "wl_output":
            if globobj.upstream.display == cdisp:
                wl_output_DROP(globobj)
        elif globname == "wl_shm":
            wl_shm_DROP(globobj, cdisp)
        elif globname == "wl_compositor":
            wl_compositor_DROP(globobj, cdisp)
        elif globname == "wl_subcompositor":
            wl_subcompositor_DROP(globobj, cdisp)
        elif globname == "zwp_linux_dmabuf_v1":
            zwp_linux_dmabuf_v1_DROP(globobj, cdisp)
        elif globname == "wl_drm":
            wl_drm_DROP(globobj, cdisp)
        elif globname == "xdg_wm_base":
            xdg_wm_base_DROP(globobj, cdisp)
        elif globname == "zxdg_decoration_manager_v1":
            zxdg_decoration_manager_v1_DROP(globobj, cdisp)
        elif globname == "zwp_relative_pointer_manager_v1":
            zwp_relative_pointer_manager_v1_DROP(globobj, cdisp)
        elif globname == "zwp_pointer_constraints_v1":
            zwp_pointer_constraints_v1_DROP(globobj, cdisp)
        elif globname == "zwp_primary_selection_device_manager_v1":
            zwp_primary_selection_device_manager_v1_DROP(globobj, cdisp)
        elif globname == "wl_data_device_manager":
            wl_data_device_manager_DROP(globobj, cdisp)
        else:
            raise NotImplementedError("Unhandled global deletion", globname)


def wl_registry_CLEAR(wl_registry, sdisp):
    for globobj in wl_registry.created_globals:
        globname = globobj.global_name
        if globname == "wl_seat":
            wl_seat_CLEAR(globobj, sdisp)
        elif globname == "wl_output":
            wl_output_CLEAR(globobj, sdisp)
        elif globname == "wl_shm":
            wl_shm_CLEAR(globobj, sdisp)
        elif globname == "wl_compositor":
            wl_compositor_CLEAR(globobj, sdisp)
        elif globname == "wl_subcompositor":
            wl_subcompositor_CLEAR(globobj, sdisp)
        elif globname == "zwp_linux_dmabuf_v1":
            zwp_linux_dmabuf_v1_CLEAR(globobj, sdisp)
        elif globname == "xdg_wm_base":
            xdg_wm_base_CLEAR(globobj, sdisp)
        elif globname == "zxdg_decoration_manager_v1":
            zxdg_decoration_manager_v1_CLEAR(globobj, sdisp)
        elif globname == "zwp_relative_pointer_manager_v1":
            zwp_relative_pointer_manager_v1_CLEAR(globobj, sdisp)
        elif globname == "zwp_pointer_constraints_v1":
            zwp_pointer_constraints_v1_CLEAR(globobj, sdisp)
        elif globname == "zwp_primary_selection_device_manager_v1":
            zwp_primary_selection_device_manager_v1_CLEAR(globobj, sdisp)
        elif globname == "wl_data_device_manager":
            wl_data_device_manager_CLEAR(globobj, sdisp)
        else:
            raise NotImplementedError("Unhandled global downstream clearing", globname)


def wl_display_DROP(wl_display, cdisp):
    del wl_display.upstreams[cdisp]

    wl_registry_DROP(wl_display.registry, cdisp)

    for callback in wl_display.sync_callbacks:
        wl_callback_sync_DROP(callback, cdisp)


def wl_display_CLEAR(wl_display, sdisp):
    wl_display.downstreams.remove(sdisp)

    for callback in wl_display.sync_callbacks:
        if callback.display == sdisp:
            # todo: how to cancel/ignore properly
            pass
    wl_display.sync_callbacks = [
        x for x in wl_display.sync_callbacks if x.display != sdisp
    ]

    wl_registry_CLEAR(wl_display.registry, sdisp)


def wl_display_ADD(wl_display, cdisp):
    assert cdisp not in wl_display.upstreams
    cdisp.downstream = wl_display
    wl_display.upstreams[cdisp] = cdisp

    wl_registry_ADD(wl_display.registry, cdisp)

    for callback in wl_display.sync_callbacks:
        wl_callback_sync_ADD(callback, cdisp)


class WaylandProxy:
    def __init__(self, comm_pipe, protocols, close_on_empty, randomize_globals):
        wp_base = [p for p in protocols if p.name == "wayland"][0]

        self.close_on_empty = close_on_empty
        self.comm_pipe = comm_pipe

        self.ServerDisplay = wayland.server.MakeDisplay(wp_base)
        self.ClientDisplay = wayland.client.MakeDisplay(wp_base)

        # Event loop stuff
        self.rdlist = []
        self.preselectlist = []
        # watch own comm fd
        self.rdlist.append(self)

        self.loop_hooks = []

        interfaces = {}
        for wp in protocols:
            for k, v in wp.interfaces.items():
                interfaces[k] = v
        self.interfaces = interfaces

        # Serial conversion. To keep track of which serial number came from where, we maintain
        # a table converting (serial,client display) for upstreams to a sequential serial for downstreams
        self.serial_list = []

        # One way to think of wl_display is as a special static global. After all,
        # sync(), delete_id(), get_registry() could just as well be provided by one,
        # and there's nothing except sanity preventing us from making it bindable
        self.display_global = StaticGlobal("wl_display")
        self.display_global.sync_callbacks = []
        self.display_global.proxy = self
        reg = StaticGlobal("wl_registry")
        self.display_global.registry = reg

        reg.advertised_globals = []

        # We advertise precisely the globals which we support, at our versions.
        # If the _upstream_ does not support these globals, depending on what the flaw is,
        # we either kill the upstream, or do a workaround
        reg.supported_global_versions = {
            "wl_shm": 1,
            "wl_compositor": 4,
            "wl_subcompositor": 1,
            "xdg_wm_base": 2,
            "zwp_linux_dmabuf_v1": 3,
            "zxdg_decoration_manager_v1": 1,
            "wl_data_device_manager": 3,
            "zwp_relative_pointer_manager_v1": 1,
            "zwp_pointer_constraints_v1": 1,
            "zwp_primary_selection_device_manager_v1": 1,
            "wl_drm": 2,
            "wl_seat": 7,
            "wl_output": 3,
        }
        globorder = [
            "wl_shm",
            "wl_drm",
            "wl_compositor",
            "wl_subcompositor",
            "xdg_wm_base",
            "zwp_linux_dmabuf_v1",
            "zxdg_decoration_manager_v1",
            "wl_data_device_manager",
            "zwp_relative_pointer_manager_v1",
            "zwp_pointer_constraints_v1",
            "zwp_primary_selection_device_manager_v1",
        ]
        # All compositors must support at least all of these
        reg.mandatory_globals = {
            "wl_shm",
            "wl_compositor",
            "wl_subcompositor",
            "xdg_wm_base",
            "zwp_linux_dmabuf_v1",
        }

        if randomize_globals:
            # Shuffle order of global presentation, to see if any program depends on it
            random.shuffle(globorder)
        for i, glob in enumerate(globorder):
            reg.advertised_globals.append((glob, None))
        # The set of currently active global objects
        reg.created_globals = []
        reg.interfaces = self.interfaces

        self.client_displays = []
        self.client_counter = 0
        self.server_displays = []
        self.server_counter = 0

    def upconv(self, serial):
        """down serial -> (up serial, client display)"""
        return self.serial_list[serial]

    def downconv(self, serial, client_display):
        """(up serial, client display) -> down serial ->"""
        ns = len(self.serial_list)
        self.serial_list.append((serial, client_display))
        return ns

    def next_serial(self):
        ns = len(self.serial_list)
        self.serial_list.append((ns, None))
        return ns

    def add_server(self, socket):
        main_display = self.ServerDisplay(
            socket, self.interfaces, prefix="sr" + str(self.server_counter)
        )
        self.server_counter += 1

        self.loop_hooks.append(LoopHook(main_display, self))
        self.rdlist.append(self.loop_hooks[-1])
        self.preselectlist.append(self.loop_hooks[-1]._preselect)

        main_display.dispatcher["get_registry"] = wl_display_REQ_get_registry
        main_display.dispatcher["sync"] = wl_display_REQ_sync

        main_display.serial_upshift = self.upconv
        main_display.serial_downshift = self.downconv
        main_display.next_down_serial = self.next_serial

        main_display.upstream = self.display_global
        self.display_global.downstreams.append(main_display)

    def remove_server(self, shortname):
        the_disp = None
        for sdisp in self.display_global.downstreams:
            if sdisp.prefix == shortname:
                the_disp = sdisp
        if not the_disp:
            print("Cannot find server display", shortname, "to remove")
            return

        for i in range(len(self.loop_hooks)):
            if self.loop_hooks[i].display == the_disp:
                tl = self.loop_hooks[i]
                self.rdlist.remove(tl)
                self.preselectlist.remove(tl._preselect)
                self.loop_hooks.remove(tl)
                break

        wl_display_CLEAR(self.display_global, the_disp)
        the_disp.disconnect()

        print("Server ", shortname, "has been removed")

    def add_client(self, display_name):
        if type(display_name) == str:
            fdconn = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM, 0)
            path = os.path.join(os.getenv("XDG_RUNTIME_DIR"), display_name)
            print("Connecting to display at", path)
            try:
                fdconn.connect(path)
            except (FileNotFoundError, ConnectionRefusedError):
                print("Failed to connect to display, skipping")
                return
        else:
            fdconn = socket.socket(fileno=display_name)

        shortname = "cl" + str(self.client_counter)
        self.client_counter += 1

        cdisp = self.ClientDisplay(fdconn, prefix=shortname)

        cdisp.serial_upshift = self.upconv
        cdisp.serial_downshift = self.downconv
        cdisp.next_down_serial = self.next_serial

        cdisp.interfaces = self.interfaces
        cdisp.proxy = self
        self.loop_hooks.append(LoopHook(cdisp, self))
        self.rdlist.append(self.loop_hooks[-1])
        self.preselectlist.append(self.loop_hooks[-1]._preselect)

        wl_display_ADD(self.display_global, cdisp)

        print("New display connection:", shortname)

    def remove_client(self, shortname):
        the_disp = None
        for cdisp in self.display_global.upstreams.keys():
            if cdisp.prefix == shortname:
                the_disp = cdisp
        if not the_disp:
            print("Cannot find client display", shortname, "to remove")
            return

        for i in range(len(self.loop_hooks)):
            if self.loop_hooks[i].display == the_disp:
                tl = self.loop_hooks[i]
                self.rdlist.remove(tl)
                self.preselectlist.remove(tl._preselect)
                self.loop_hooks.remove(tl)
                break

        # Recursively drop everything that was associated to the upstream, and trigger any live callbacks
        # that become due because the upstream was removed
        wl_display_DROP(self.display_global, the_disp)
        the_disp.disconnect()

        print("Client ", shortname, "has been removed")

    def fileno(self):
        return self.comm_pipe.fileno()

    def doread(self):
        command = self.comm_pipe.readline().strip()
        if not command:
            return
        r = "\33[32m"
        n = "\33[0m"
        print(r + "Received Command:" + n, command)
        if command.startswith("connect "):
            command = command[8:]
            print(r + "Trying to add client" + n)
            self.add_client(command)

        elif command.startswith("drop cl"):
            command = command[7:]
            try:
                clno = int(command)
            except ValueError:
                print(
                    r
                    + "Invalid command, should have format `drop cl<i>` where <i> is display number"
                    + n
                )
            self.remove_client("cl" + str(command))
        else:
            print(
                r + "Invalid command, must have format `drop cl<i>` or `connect p`" + n
            )

    def eventloop(self):
        self.shutdowncode = None
        while self.shutdowncode is None:
            if self.close_on_empty and not self.display_global.downstreams:
                print(self.close_on_empty, self.display_global.downstreams)
                break

            prelist = self.preselectlist.copy()
            for i in prelist:
                i()
            try:
                (rd, wr, ex) = select.select(self.rdlist, [], [])
            except KeyboardInterrupt:
                print("KeyboardInterrupt")
                break
            for i in rd:
                i.doread()


class BoundUnixServerSocket:
    def __init__(self, path):
        self.path = path

    def __enter__(self):
        self.server_socket = socket.socket(
            family=socket.AF_UNIX, type=socket.SOCK_STREAM
        )
        self.server_socket.bind(self.path)
        self.server_socket.listen()
        return self.server_socket

    def __exit__(self, e_type, e_val, e_tb):
        self.server_socket.close()
        os.remove(self.path)


def load_protocols():
    wayland_dir = "/usr/share/wayland/"
    wayland_protocols_dir = "/usr/share/wayland-protocols/"
    wp_base = wayland.protocol.Protocol(os.path.join(wayland_dir, "wayland.xml"))
    wp_xdg_shell = wayland.protocol.Protocol(
        os.path.join(wayland_protocols_dir, "stable/xdg-shell/xdg-shell.xml"),
        parent=wp_base,
    )
    zwp_linux_dmabuf_v1 = wayland.protocol.Protocol(
        os.path.join(
            wayland_protocols_dir, "unstable/linux-dmabuf/linux-dmabuf-unstable-v1.xml"
        ),
        parent=wp_base,
    )
    xdg_decoration_unstable_v1 = wayland.protocol.Protocol(
        os.path.join(
            wayland_protocols_dir,
            "unstable/xdg-decoration/xdg-decoration-unstable-v1.xml",
        ),
        parent=wp_xdg_shell,
    )
    wp_primary_selection_unstable_v1 = wayland.protocol.Protocol(
        os.path.join(
            wayland_protocols_dir,
            "unstable/primary-selection/primary-selection-unstable-v1.xml",
        ),
        parent=wp_base,
    )
    pointer_constraints_unstable_v1 = wayland.protocol.Protocol(
        os.path.join(
            wayland_protocols_dir,
            "unstable/pointer-constraints/pointer-constraints-unstable-v1.xml",
        ),
        parent=wp_base,
    )
    relative_pointer_unstable_v1 = wayland.protocol.Protocol(
        os.path.join(
            wayland_protocols_dir,
            "unstable/relative-pointer/relative-pointer-unstable-v1.xml",
        ),
        parent=wp_base,
    )
    wayland_drm = wayland.protocol.Protocol(
        os.path.join(
            os.path.dirname(os.path.realpath(__file__)), "protocols/wayland-drm.xml"
        ),
        parent=wp_base,
    )
    return [
        wp_base,
        wp_xdg_shell,
        zwp_linux_dmabuf_v1,
        xdg_decoration_unstable_v1,
        wayland_drm,
    ]


def run_single(args, protocols, comm_pipe, client_display_names, program):
    sockets = socket.socketpair(socket.AF_UNIX, socket.SOCK_STREAM)
    child_env = os.environ.copy()
    for x in ["WAYLAND_DISPLAY", "WAYLAND_SOCKET", "DISPLAY"]:
        if x in child_env:
            del child_env[x]
    child_env["WAYLAND_SOCKET"] = str(sockets[1].fileno())
    proc = subprocess.Popen(program, env=child_env, pass_fds=[sockets[1].fileno()])
    sockets[1].close()

    wayland_proxy = WaylandProxy(comm_pipe, protocols, True, args.randomize_globals)
    wayland_proxy.add_server(sockets[0])
    for c in client_display_names:
        wayland_proxy.add_client(c)

    wayland_proxy.eventloop()

    proc.wait()


def get_random_wayland_socket_name():
    chars = "0123456789abcdefghijklmnopqrstuvwxyz"
    return "wmux-display-" + "".join(chars[random.randrange(36)] for i in range(8))


def run_fork(args, protocols, comm_pipe, client_display_names, program):
    dispname = get_random_wayland_socket_name()
    xdg_runtime_dir = os.getenv("XDG_RUNTIME_DIR")
    display_path = os.path.join(xdg_runtime_dir, dispname)

    proc_env = os.environ.copy()
    for x in ["WAYLAND_DISPLAY", "WAYLAND_SOCKET", "DISPLAY"]:
        if x in proc_env:
            del proc_env[x]
    proc_env["WAYLAND_DISPLAY"] = dispname

    child_pipes = []

    with BoundUnixServerSocket(display_path) as server_socket:
        proc = subprocess.Popen(program, env=proc_env)

        while True:
            retcode = proc.poll()
            if retcode is not None:
                break

            rd, wr, ex = select.select([comm_pipe, server_socket], [], [])
            if server_socket in rd:
                conn, addr = server_socket.accept()
                rdpipe, wrpipe = os.pipe2(os.O_NONBLOCK)
                child_pipes.append(os.fdopen(wrpipe, mode="w"))
                # todo: it's probably preferable to spin up a fully independent
                # process here, and explicitly pass in the bits we want to inherit.
                pid = os.fork()
                if pid == 0:
                    server_socket.close()
                    comm_pipe.close()
                    os.close(wrpipe)

                    try:
                        # Q: do 'wmux-socket'?
                        wayland_proxy = WaylandProxy(
                            os.fdopen(rdpipe, mode="r"),
                            protocols,
                            True,
                            args.randomize_globals,
                        )
                        wayland_proxy.add_server(conn)
                        for c in client_display_names:
                            wayland_proxy.add_client(c)

                        wayland_proxy.eventloop()
                    except:
                        import traceback

                        print(traceback.format_exc())

                    # to not trigger any cleanup handlers early
                    os._exit(0)
                else:
                    os.close(rdpipe)
                    conn.close()

            if comm_pipe in rd:
                command = comm_pipe.readline().strip()
                if command:
                    print("Root process received command:", command)
                    dead_pipes = []
                    for pipe in child_pipes:
                        try:
                            pipe.write(command + "\n")
                            pipe.flush()
                        except BrokenPipeError:
                            dead_pipes.append(pipe)
                    child_pipes = [p for p in child_pipes if p not in dead_pipes]

        proc.wait()


def run_composite(args, protocols, comm_pipe, client_display_names, program):
    dispname = get_random_wayland_socket_name()
    xdg_runtime_dir = os.getenv("XDG_RUNTIME_DIR")
    display_path = os.path.join(xdg_runtime_dir, dispname)

    proc_env = os.environ.copy()
    for x in ["WAYLAND_DISPLAY", "WAYLAND_SOCKET", "DISPLAY"]:
        if x in proc_env:
            del proc_env[x]
    proc_env["WAYLAND_DISPLAY"] = dispname

    wayland_proxy = WaylandProxy(comm_pipe, protocols, False, args.randomize_globals)
    for c in client_display_names:
        wayland_proxy.add_client(c)

    with BoundUnixServerSocket(display_path) as server_socket:
        proc = subprocess.Popen(program, env=proc_env)

        class Joiner:
            def fileno(self):
                return server_socket.fileno()

            def doread(self):
                conn, addr = server_socket.accept()
                wayland_proxy.add_server(conn)

        new_conn = Joiner()
        wayland_proxy.rdlist.append(new_conn)

        wayland_proxy.eventloop()

        proc.wait()


def main():
    # Usage: ./wmux.py --verbose -d wayland-1 --display wayland-7 -- weston-terminal
    parser = argparse.ArgumentParser(
        description="Forward a wayland client program to a wayland compositor, perhaps with protocol edits"
    )
    parser.add_argument(
        "-d", "--display", help="Wayland display to connect to", action="append"
    )
    parser.add_argument("program", nargs="+", help="command to run")
    parser.add_argument(
        "--local",
        action="count",
        help="Connect to the value of env WAYLAND_DISPLAY",
    )
    parser.add_argument(
        "-s",
        "--single",
        help="Provide single-use socket to child program",
        action="store_true",
    )
    parser.add_argument(
        "-f",
        "--fork",
        help="Provide a multi-use display socket to child program, forking per connection",
        action="store_true",
    )
    parser.add_argument(
        "-c",
        "--composite",
        help="Provide a multi-use display socket to child program, combining all connections",
        action="store_true",
    )
    parser.add_argument(
        "--randomize-globals",
        help="Randomly shuffle the order in which globals are presented",
        action="store_true",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Verbose output",
    )

    # todo: feature, expanding a single-use socket to be usable by multiple programs...

    args = parser.parse_args()
    if (
        (args.single and args.composite)
        or (args.single and args.fork)
        or (args.fork and args.composite)
    ):
        print("Only one of --composite, --fork, --single can be chosen")
        quit()

    if args.single:
        mode = "single"
    elif args.composite:
        mode = "composite"
    else:
        mode = "fork"

    displays = args.display
    program = args.program
    randomize_globals = args.randomize_globals
    # technically we should look at the order, so --local,--display,--local assigns cl0,cl1,cl2
    if displays is None:
        displays = []
    if args.local is not None and args.local > 0:
        dispv = os.getenv("WAYLAND_DISPLAY")
        sockv = os.getenv("WAYLAND_SOCKET")
        if mode == "fork" and dispv is None and sockv is not None:
            raise Exception("Cannot use fork-mode with a local upstream socket")
        sockname = dispv if dispv is not None else int(sockv)
        displays += [sockname] * args.local
    print("Arguments:", mode, displays, program)

    # TODO: make filter that prints just 'ignore' events, for example...
    logging.basicConfig(level=(logging.INFO if args.verbose else logging.WARNING))

    protocols = load_protocols()

    try:
        os.remove("wmux_comm_pipe")
    except FileNotFoundError:
        pass
    os.mkfifo("wmux_comm_pipe")
    comm_pipe = os.fdopen(os.open("wmux_comm_pipe", os.O_RDONLY | os.O_NONBLOCK))

    if mode == "single":
        run_single(args, protocols, comm_pipe, displays, program)
    elif mode == "composite":
        run_composite(args, protocols, comm_pipe, displays, program)
    elif mode == "fork":
        run_fork(args, protocols, comm_pipe, displays, program)
    else:
        raise NotImplementedError()


if __name__ == "__main__":
    main()
